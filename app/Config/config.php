<?php
/**
 * This is original configuration file.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Config
 * @since       yamatoya.sys v 1.0
 */

	/**
	 * Site Setting:
	 *
	 * @var array
	 */
	$config = array(
		'Setting' => array(
			'site-name'   => '大和屋本店ウェディング予約システム',
			'description' => '',
			'keywords'    => '',
			'copyright'   => 'Copyright &copy; 2012-' . date('Y') . ' Yamatoya-Honten Co.,Ltd. All rights reserved.',
		),
	);
