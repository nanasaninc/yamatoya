<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
	Router::connect('/auth',       array('controller' => 'yamatoya', 'action' => 'auth'));
	Router::connect('/unauth',     array('controller' => 'yamatoya', 'action' => 'unauth'));

	Router::connect('/',           array('controller' => 'yamatoya', 'action' => 'index'));
	Router::connect('/course/*',   array('controller' => 'yamatoya', 'action' => 'index', 'course'));
	Router::connect('/japanese/*', array('controller' => 'yamatoya', 'action' => 'index', 'japanese'));
	Router::connect('/french/*',   array('controller' => 'yamatoya', 'action' => 'index', 'french'));
	Router::connect('/chinese/*',  array('controller' => 'yamatoya', 'action' => 'index', 'chinese'));
	Router::connect('/select/*',   array('controller' => 'yamatoya', 'action' => 'index', 'select'));
	Router::connect('/confirm/*',  array('controller' => 'yamatoya', 'action' => 'confirm'));
	Router::connect('/complete/*', array('controller' => 'yamatoya', 'action' => 'complete'));
	Router::connect('/printing/*', array('controller' => 'yamatoya', 'action' => 'printing'));

/**
 * CakePHP Install check page.
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/check/*', array('controller' => 'pages', 'action' => 'display', 'home'));
//	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

/**
 * Debug level switch of nanasan access.
 * @ado nanasanから確認すると強制でデバッグモードになる
 *      IPが変わった場合は要修正
 */
	App::uses('CakeRequest', 'Network');
	if (CakeRequest::clientIp(true) == '219.117.220.11') {
		Configure::write('debug', 2);
	}
