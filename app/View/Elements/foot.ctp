<?php
/**
 * Element block - Foot
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Elements
 * @since       yamatoya.sys v 1.0
 */
?><!-- Scripts -->
<script src="<?php echo $this->Html->url('/js/jquery.fancybox.js?v=1.3.4', true); ?>"></script>
<script src="<?php echo $this->Html->url('/js/jquery.jscrollpane.js?v=2.0.0b12', true); ?>"></script>
<script src="<?php echo $this->Html->url('/js/jquery.mousewheel.js?v=3.0.6', true); ?>"></script>
<script src="<?php echo $this->Html->url('/js/flipsnap.js?v=0.6.0', true); ?>"></script>
<script src="<?php echo $this->Html->url('/js/common.js', true); ?>"></script>
<?php echo $this->fetch('script'); ?>