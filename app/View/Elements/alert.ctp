<?php
/**
 * Element block - Alert
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://ssl-kion.jp/ Kion
 * @package     app.View.Elements
 * @since       Kion v 1.0
 */
?><div class="alert<?php echo isset($class)? ' alert-' . $class: ''; ?>"><?php
	echo $message;
?></div>