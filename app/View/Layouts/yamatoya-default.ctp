<?php
/**
 * Layout frame - Default
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Layouts
 * @since       yamatoya.sys v 1.0
 */
?><!DOCTYPE html>
<html class="no-js" lang="ja">
<?php echo $this->element('head') . PHP_EOL; ?>
<body<?php if (isset($body_id)) echo " id=\"{$body_id}\""; ?>>
<?php echo $this->fetch('content') . PHP_EOL; ?>
<div class="mask">
	<div class="loader">
		<img src="<?php echo $this->Html->url('/img/common/ajax-loader.gif', true); ?>" alt="Loading..." width="32" height="32">
		<p>ページを読み込み中です。</p>
	</div><!-- /.loader -->
</div><!-- /.mask -->
<?php echo $this->element('foot') . PHP_EOL; ?>
</body>
</html>