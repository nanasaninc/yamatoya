<?php
/**
 * View: Menu - Course
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', 'コースメニュー選択画面');

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<div id="container" class="course-top">
		<div id="ja" class="block">
			<a href="<?php echo $this->Html->url("/japanese", true); ?>" class="applink">
				<p>日本料理<br><span class="en">Japanese</span></p>
			</a>
		</div><!-- /#ja.block -->
		<div id="fr" class="block">
			<a href="<?php echo $this->Html->url("/french", true); ?>" class="applink">
				<p>西洋料理<br><span class="en">French</span></p>
			</a>
		</div><!-- /#fr.block -->
		<div id="ch" class="block">
			<a href="<?php echo $this->Html->url("/chinese", true); ?>" class="applink">
				<p>中国料理<br><span class="en">Chinese</span></p>
			</a>
		</div><!-- /#ch.block -->
		<p class="logo"><a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/logo.png", true); ?>" width="85" height="20"></a></p>
		<div class="homeBtn"><a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/icon-home.png", true); ?>" width="42" height="42" alt="トップへ戻る"></a></div>
	</div><!-- /#container -->
