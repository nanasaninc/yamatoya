<?php
/**
 * View: Menu - Select
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', 'セレクトメニュー選択画面');

	/**
	 * Folder class activated.
	 */
	App::uses('Folder', 'Utility');
	$folder_p = new Folder(APP . "webroot/files/products/");

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function() {
	// Loader position.
	var window_height = $(window).height(),
		loader_height = $('.mask .loader').height();
	$('.mask .loader').css({ 'margin-top': ((window_height - loader_height) / 2), 'opacity': 1 });
	// FlipSnap Form
	var num   = 0,
	    width = $('#OrderIndexForm').width(),
	    fsnap = Flipsnap('#flipform', { distance: width });
	$('.detail').css({
		'margin-left':  width * 0.05,
		'margin-right': width * 0.05,
		'width':        width * 0.9,
		'opacity':      1
	});
	$('#flipform').width(width * ($('.detail').index() + 1));
	// Window resized.
	$(window).on('orientationchange resize', function() {
		$('#flipform').fadeOut(0, function() {
			width = $('#OrderIndexForm').width(),
			fsnap = Flipsnap('#flipform', { distance: width });
			$('.detail').css({
				'margin-left':  width * 0.05,
				'margin-right': width * 0.05,
				'width':        width * 0.9,
				'opacity':      1
			});
			$('#flipform').width(width * ($('.detail').index() + 1));
			fsnap.moveToPoint(num);
			$('#flipform').fadeIn(0);
		});
	});
	// FlipSnap event.
	fsnap.element.addEventListener('fstouchend', function(ev) {
		num = ev.newPoint;
		$('#message').empty();
	}, false);
<?php if (isset($p)): ?>
	// Re-select page changer.
	num = $('.detail').index($('#p<?php echo $p; ?>'));
	fsnap.moveToPoint(num);
<?php endif; ?>
	// Lineup list click.
	$('#lineup li').click(function() {
		num = $('#lineup li').index(this);
		fsnap.moveToPoint(num);
		$('#message').empty();
	});
	// Back button.
	$('.back-button').click(function() {
		if (num == 0) {
			location.href = "<?php echo $this->Html->url('/', true); ?>";
		} else {
			num--;
			fsnap.moveToPoint(num);
			$('#message').empty();
		}
	});
	// Next button.
	$('.next-button').click(function() {
		if (($('.detail').index()) <= num) {
			$('form').submit();
		} else {
			num++;
			fsnap.moveToPoint(num);
			$('#message').empty();
		}
	});
	// Validate alert.
	$('form').submit(function() {
		var radio = $('<div/>').addClass('alert').addClass('alert-error').text('選択してください。'),
			check = $('<div/>').addClass('alert').addClass('alert-error').text('選択出来るのは３品までです。');
<?php $i = 0; foreach ($menus as $menu_type_id => $type_menus): if (!$menu_type_flags[$menu_type_id]): ?>
		if ($('.radio<?php echo $menu_type_id; ?>:checked').val() == undefined) {
			num = <?php echo $i; ?>;
			fsnap.moveToPoint(num);
			$('#message').html(radio);
			return false;
		}
<?php else: ?>
		if ($('.check<?php echo $menu_type_id; ?>:checked').length > 3) {
			num = <?php echo $i; ?>;
			fsnap.moveToPoint(num);
			$('#message').html(check);
			return false;
		}
<?php endif; $i++; endforeach; ?>
	});
	// Total plice calc.
	var number_format = function(num) {
			return num.toString().replace(/^\d+[^\.]/, function(t) {
				return t.replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t) {
					return t + ',';
				});
			});
		},
		price_calc = function() {
		var price   = 11400,
			checked = $(':radio:checked, :checkbox:checked');
		$.each(checked, function() {
			var menu_id          = $(this).val(),
				difference_price = parseInt($('#OrderPrice' + menu_id).val());
			price += difference_price;
		});
		// 3桁毎にコンマ挿入
		$('.total').text(number_format(price));
	};
	$(':radio, :checkbox').change(function() {
		price_calc();
	});
	$(window).load(function() {
		price_calc();
	});
});
</script>
<?php $this->end(); ?>

	<div id="wrapper">
		<div id="container">
			<div id="primary">
			<div id="message">
				<?php echo $this->Session->flash() . PHP_EOL; ?>
			</div><!-- /#message -->
			<?php echo $this->Form->create('Order') . PHP_EOL; ?>
				<div id="flipform">
<?php $count = $num = 0; foreach ($menus as $menu_type_id => $type_menus): ?>
					<div class="detail" id="p<?php echo $menu_type_id; ?>">
						<div class="headBox">
							<h1><?php echo h($menu_type_names[$menu_type_id]); ?></h1>
						</div>
						<p class="totalPrice">
							<span class="total">11,400</span>円<br>
							<span class="tax">(税金・サービス料別)</span>
						</p>
						<div class="selectBox num<?php echo $count; ?>">
<?php foreach ($type_menus as $key => $menu) {
		if ($menu['Menu']['deleted'] == 0 ) { ?>
							<div class="menu">
								<label>
									<div class="button" style="width:18px;text-align:center"><?php
										if (!$menu['MenuType']['multiple'] && !$menu['Menu']['required']) {
											$options = array(
												'type'        => 'radio',
												'options'     => array($menu['Menu']['id'] => ''),
												'hiddenField' => false,
												'class'       => "radio{$menu_type_id}",
												'label'       => false,
												'legend'      => false,
												'div'         => false,
											);
											if (array_search($menu['Menu']['id'], $this->request->data['Session']['Order']['menus']) !== false) {
												$options['value'] = $menu['Menu']['id'];
											}
											echo $this->Form->input("OrderMenu.{$num}.menu_id", $options);
										} else {
											if ($menu['Menu']['required'])
												$num++;
											$options = array(
												'value'       => $menu['Menu']['id'],
												'hiddenField' => false,
												'class'       => "check{$menu_type_id}",
												'label'       => false,
												'div'         => false,
												'checked'     => (boolean)$menu['Menu']['required'],
											);
											if ((boolean)$menu['Menu']['required']) {
												// Required menu.
												$options['checked'] = true;
												// $options['onclick'] = 'return false;';
												// $options['class']   = 'hide';
											} else {
												// Check selected menu.
												if (array_search($menu['Menu']['id'], $this->request->data['Session']['Order']['menus']) !== false) {
													$options['checked'] = true;
												}
											}
											echo $this->Form->checkbox("OrderMenu.{$num}.menu_id", $options);
											$num++;
										}
										echo $this->Form->hidden("price_{$menu['Menu']['id']}", array('value' => $menu['Menu']['difference_price']));
									?></div><!-- /.button -->
									<div class="thumbnail"><?php
										$files = $folder_p->find("{$menu['MenuProduct'][0]['Product']['image']}@th.jpg");
										$image = !empty($files)?
											"/files/products/{$files[0]}":
											"/img/common/nowprinting-product@th.png";
										echo "<a href=\"#gallery{$count}\" class=\"popup\"><img src=\"" . $this->Html->url($image, true) . "\" width=\"70\" height=\"70\"></a>";
										$galleries[$count][] = array(
											'name'      => $menu['Menu']['name'],
											'image'     => str_replace('@th', '' , $image),
											'type_name' => !empty($menu['MenuProduct'][0]['Product']['ProductType'])? $menu['MenuProduct'][0]['Product']['ProductType']['name']: null,
											'type_slug' => !empty($menu['MenuProduct'][0]['Product']['ProductType'])? $menu['MenuProduct'][0]['Product']['ProductType']['slug']: null,
											'price'     => $menu['Menu']['difference_price'],
										);
									?></div><!-- /.thumbnail -->
									<dl>
										<dt><?php
											echo !empty($menu['MenuProduct'][0]['Product']['ProductType'])? "<span class=\"icon {$menu['MenuProduct'][0]['Product']['ProductType']['slug']}\">{$menu['MenuProduct'][0]['Product']['ProductType']['name']}</span>": null;
											echo h($menu['Menu']['name']);
											if ($menu['Menu']['difference_price'] > 0) {
												echo "<span class=\"plus\">(+" . $this->Number->currency($menu['Menu']['difference_price']) . ")</span>";
											}
										?></dt>
										<dd><?php echo !empty($menu['MenuProduct'][0]['Product']['description'])? n2b_h($menu['MenuProduct'][0]['Product']['description']): '&nbsp;'; ?></dd>
									</dl>
								</label>
							</div><!-- /.menu -->
							<?php }} ?>
						</div><!-- /.selectBox -->
					</div><!-- /.detail -->
<?php $count++; $num++; endforeach; ?>
				</div><!-- /#flipform -->
				<ul class="buttons">
					<li class="notice">※季節によって内容が変わる場合がございます。ご了承ください。</li>
					<li class="button back">
						<a href="#" class="back-button applink"><span>戻る</span></a>
					</li><!-- /.button.back -->
					<li class="button next">
						<a href="#" class="next-button applink"><span>次へ</span></a>
					</li><!-- /.button.back -->
					<li class="button select"><?php
						echo $this->Form->hidden('Order.menu_type_id', array(
							'value' => $type_id,
							'div'   => false,
						));
						echo $this->Form->submit('メニュー確認', array(
							'div'   => false,
						));
					?></li><!-- /.button.select -->
				</ul><!-- /.buttons -->
			<?php echo $this->Form->end() . PHP_EOL; ?>
			</div><!-- /#primary -->

			<div id="secondary">
				<h2>Select Menu</h2>
				<ul id="lineup">
<?php foreach ($menus as $menu_type_id => $type_menus): ?>
					<li><?php echo h($menu_type_names[$menu_type_id]); ?></li>
<?php endforeach; ?>
				</ul>
<?php if (!empty($this->request->data['Session']['OrderMenu'])): ?>
				<div class="btn-confirm">
					<a href="<?php echo $this->Html->url("/confirm?back=Select", true); ?>" class="applink">確認</a>
				</div>
<?php endif; ?>
				<div class="btn_popupImg">
					<div class="ossm">
						<a href="<?php echo $this->Html->url("/", true); ?>img/ossm.jpg" class="popup">オススメセレクト</a>
					</div>
					<div class="okosama">
						<a href="<?php echo $this->Html->url("/", true); ?>img/okosama.jpg" class="popup">お子様料理メニュー</a>
					</div>
				</div>
				<div class="logo">
					<a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/logo.png", true); ?>" width="85" height="20"></a>
				</div>
			</div><!-- /#secondary -->
			<div class="homeBtn"><a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/icon-home.png", true); ?>" width="42" height="42" alt="トップへ戻る"></a></div>

<?php foreach ($galleries as $count => $images): ?>
			<div class="hide">
				<div id="gallery<?php echo $count; ?>" class="gallery">
					<div class="gallery-wrap">
						<div class="viewport">
							<div class="flipsnap">
<?php foreach ($images as $key => $image): ?>
								<div class="item">
<?php if (!empty($image['type_name'])): ?>
									<div class="icon <?php echo $image['type_slug']; ?>"><?php echo $image['type_name']; ?></div>
<?php endif; ?>
<?php if ($image['price']): ?>
									<div class="plus">
										<p>+<?php echo $this->Number->currency($image['price']); ?></p>
									</div>
<?php endif; ?>
									<div class="pic"><?php
										echo "<img src=\"" . $this->Html->url($image['image'], true) . "\" width=\"680\" height=\"680\">";
									?></div>
									<p><?php echo h($image['name']); ?></p>
								</div>
<?php endforeach; ?>
							</div>
						</div>
					</div><!-- /.gallery-wrap -->
				</div><!-- /#gallery<?php echo $key; ?> -->
			</div><!-- /.hide -->
<?php endforeach; ?>

		</div><!-- /#container -->

	</div><!-- /#wrapper -->
