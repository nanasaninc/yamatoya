<?php
/**
 * View: Menu - Course: Japanese
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya.course
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', 'コースメニュー選択画面');

	/**
	 * Folder class activated.
	 */
	App::uses('Folder', 'Utility');
	$folder_m = new Folder(APP . "webroot/files/menus/");
	$folder_p = new Folder(APP . "webroot/files/products/");

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function() {
	// Loader position.
	var window_height = $(window).height(),
		loader_height = $('.mask .loader').height();
	$('.mask .loader').css({ 'margin-top': ((window_height - loader_height) / 2), 'opacity': 1 });
	// FlipSnap Form
	var num   = 0,
	    width = $('#OrderIndexForm').width(),
	    fsnap = Flipsnap('#flipform', { distance: width });
	$('.detail').css({
		'margin-left':  width * 0.05,
		'margin-right': width * 0.05,
		'width':        width * 0.9,
		'opacity':      1
	});
	$('#flipform').width(width * ($('.detail').index() + 1));
	$('#OrderMenu0MenuId').val($('#OrderMenuId'+num).val());
	// Window resized.
	$(window).on('orientationchange resize', function() {
		$('#flipform').fadeOut(0, function() {
			width = $('#OrderIndexForm').width(),
			fsnap = Flipsnap('#flipform', { distance: width });
			$('.detail').css({
				'margin-left':  width * 0.05,
				'margin-right': width * 0.05,
				'width':        width * 0.9,
				'opacity':      1
			});
			$('#flipform').width(width * ($('.detail').index() + 1));
			fsnap.moveToPoint(num);
			$('#flipform').fadeIn(0);
		});
	});
	// FlipSnap event.
	fsnap.element.addEventListener('fstouchend', function(ev) {
		num = ev.newPoint;
		$('#OrderMenu0MenuId').val($('#OrderMenuId'+num).val());
	}, false);
<?php if (isset($p)): ?>
	// Re-select page changer.
	num = $('.detail').index($('#p<?php echo $p; ?>'));
	fsnap.moveToPoint(num);
	$('#OrderMenu0MenuId').val($('#OrderMenuId'+num).val());
<?php endif; ?>
	// Lineup list click.
	$('#lineup li').click(function() {
		num = $('#lineup li').index(this);
		fsnap.moveToPoint(num);
		$('#OrderMenu0MenuId').val($('#OrderMenuId'+num).val());
	});
});
</script>
<?php $this->end(); ?>

	<div id="wrapper" class="<?php echo $wrap_class; ?>">
		<div id="container">
			<div id="primary">
			<?php echo $this->Form->create('Order') . PHP_EOL; ?>
				<div id="flipform">
<?php $count = 0; foreach ($menus as $key => $menu): ?>
					<div class="detail" id="p<?php echo $menu['Menu']['id']; ?>">
						<div class="headBox">
							<h1><?php echo $menu['Menu']['name']; ?></h1>
							<p class="sub"><?php echo $menu['Menu']['kana']; ?></p>
						</div>
						<p class="price"><?php
							echo $this->Number->currency($menu['Menu']['price']);
							echo $this->Form->hidden("Order.menu_id_{$count}", array(
								'value' => $menu['Menu']['id'],
								'div'   => false,
							));
						?><br>
						<span class="tax">(税金・サービス料別)</span></p>
						<div class="detailBox">
							<table>
								<tbody class="namelists num<?php echo $key; ?>">
<?php foreach ($menu['MenuProduct'] as $product): ?>
									<tr>
<?php if (!empty($product['Product']['ProductType']['name'])): ?>
										<th style="width:65px"><?php echo $product['Product']['ProductType']['name']; ?></th>
<?php endif; ?>
										<td><a href="#gallery<?php echo $menu['Menu']['id']; ?>" class="popup"><?php echo $product['Product']['name']; ?></a></td>
									</tr>
<?php endforeach; ?>
								</tbody>
							</table>
							<div class="mainImg"><?php
								$files = $folder_m->find("{$menu['Menu']['image']}.jpg");
								echo !empty($files)?
									"<a href=\"" . $this->Html->url("/files/menus/{$files[0]}", true) . "\" class=\"popup\"><img src=\"" . $this->Html->url("/files/menus/{$files[0]}", true) . "\" width=\"218\" height=\"283\"></a>":
									"<a href=\"" . $this->Html->url("/img/common/nowprinting-menu.png", true) . "\" class=\"popup\"><img src=\"" . $this->Html->url("/img/common/nowprinting-menu.png", true) . "\" width=\"218\" height=\"283\"></a>";
							?></div>
						</div>
						<div class="thumbnail num<?php echo $key; ?>">
							<ul><?php
								foreach ($menu['MenuProduct'] as $key => $product):
								?><li><?php
									$files = $folder_p->find("{$product['Product']['image']}@th.jpg");
									$image = !empty($files)?
										"/files/products/{$files[0]}":
										"/img/common/nowprinting-product@th.png";
									echo "<a href=\"#gallery{$menu['Menu']['id']}\" class=\"popup\"><img src=\"" . $this->Html->url($image, true) . "\" width=\"55\" height=\"55\"></a>";
									$galleries[$menu['Menu']['id']][] = array(
										'name'      => $product['Product']['name'],
										'image'     => str_replace('@th', '' , $image),
										'type_name' => !empty($product['Product']['ProductType'])? $product['Product']['ProductType']['name']: null,
										'type_slug' => !empty($product['Product']['ProductType'])? $product['Product']['ProductType']['slug']: null,
									);
								?></li><?php
								endforeach;
							?></ul>
						</div>
					</div><!-- /.detail -->
<?php $count++; endforeach; ?>
				</div><!-- /#flipform -->
				<ul class="buttons">
					<li class="notice">※季節によって内容が変わる場合がございます。ご了承ください。</li>
					<li class="button back">
						<a href="<?php echo $this->Html->url("/course", true); ?>" class="applink"><span>戻る</span></a>
					</li>
					<li class="button select"><?php
						echo $this->Form->hidden('Order.menu_type_id', array(
							'value' => $type_id,
							'div'   => false,
						));
						echo $this->Form->hidden('OrderMenu.0.menu_id');
						echo $this->Form->submit('このコースを選択', array(
							'div'   => false,
						));
					?></li>
				</ul>
			<?php echo $this->Form->end() . PHP_EOL; ?>
			</div><!-- /#primary -->

			<div id="secondary">
				<h2><?php echo Inflector::humanize($wrap_class); ?></h2>
				<ul id="lineup">
					<?php foreach ($menus as $key => $menu): ?>
						<li><?php echo $menu['Menu']['name']; ?></li>
					<?php endforeach; ?>
				</ul>
				<?php if (!empty($this->request->data['Session']['OrderMenu'])): ?>
					<div class="btn-confirm">
						<a href="<?php echo $this->Html->url("/confirm?back=" . Inflector::humanize($wrap_class), true); ?>" class="applink">確認</a>
					</div>
				<?php endif; ?>
				<div class="courseNav">
<?php if ($wrap_class != 'japanese'): ?>
					<div class="button-ja">
						<a href="<?php echo $this->Html->url("/japanese", true); ?>" class="applink">日本料理</a>
					</div>
<?php endif; ?>
<?php if ($wrap_class != 'french'): ?>
					<div class="button-fr">
						<a href="<?php echo $this->Html->url("/french", true); ?>" class="applink">西洋料理</a>
					</div>
<?php endif; ?>
<?php if ($wrap_class != 'chinese'): ?>
					<div class="button-ch">
						<a href="<?php echo $this->Html->url("/chinese", true); ?>" class="applink">中華料理</a>
					</div>
<?php endif; ?>
				</div>
				<div class="btn_popupImg">
					<div class="okosama">
						<a href="<?php echo $this->Html->url("/", true); ?>img/okosama.jpg" class="popup">お子様料理メニュー</a>
					</div>
				</div>
				<div class="logo">
					<a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/logo.png", true); ?>" width="85" height="20"></a>
				</div>
			</div><!-- /#secondary -->
			<div class="homeBtn"><a href="<?php echo $this->Html->url("/", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/icon-home.png", true); ?>" width="42" height="42" alt="トップへ戻る"></a></div>

<?php foreach ($galleries as $menu_id => $images): ?>
			<div class="hide">
				<div id="gallery<?php echo $menu_id; ?>" class="gallery">
					<div class="gallery-wrap">
						<div class="viewport">
							<div class="flipsnap">
<?php foreach ($images as $key => $image): ?>
								<div class="item">
<?php if (!empty($image['type_name'])): ?>
									<div class="icon <?php echo $image['type_slug']; ?>"><?php echo $image['type_name']; ?></div>
<?php endif; ?>
									<div class="pic"><?php
										echo "<img src=\"" . $this->Html->url($image['image'], true) . "\" width=\"680\" height=\"680\">";
									?></div>
									<p><?php echo $image['name']; ?></p>
								</div>
<?php endforeach; ?>
							</div>
						</div>
					</div><!-- /.gallery-wrap -->
				</div><!-- /#gallery<?php echo $menu_id; ?> -->
			</div><!-- /.hide -->
<?php endforeach; ?>

		</div><!-- /#container -->
	</div><!-- /#wrapper.japanese -->
