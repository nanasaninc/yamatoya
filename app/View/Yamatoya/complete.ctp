<?php
/**
 * View: Complete
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '登録完了');

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<div id="wrapper">
		<div id="container">
			<h1>ご予約ありがとうございます。</h1>
			<p id="text">おふたりがゲストの方々のために選んだお料理を<br>
			おふたりにかわってひと皿、ひと皿心を込めてお届けします。</p>
			<div class="buttons">
				<div class="button select">
					<a href="<?php echo $this->Html->url("/unauth", true); ?>" class="applink"><span>完了</span></a>
				</div>
			</div><!-- /.buttons -->
			<div class="logo">
				<a href="<?php echo $this->Html->url("/unauth", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/logo2.png", true); ?>" width="85" height="20" /></a>
			</div><!-- /.logo -->
		</div><!-- /#container -->
		<div class="homeBtn"><a href="<?php echo $this->Html->url("/unauth", true); ?>" class="applink"><img src="<?php echo $this->Html->url("/img/common/icon-home.png", true); ?>" width="42" height="42" alt="トップへ戻る" /></a></div>
	</div><!-- /#wrapper -->
