<?php
/**
 * View: Auth
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '');

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<div id="wrapper">
		<div id="container">
			<?php echo $this->Form->create('Order', array('class' => 'authBox')) . PHP_EOL; ?>
				<div class="input">
				<?php echo $this->Session->flash() . PHP_EOL; ?>
				<?php
					echo $this->Form->input('Order.order_name', array(
						'type'        => 'password',
						'placeholder' => '予約ID',
						'autofocus'   => true,
						'class'       => 'id',
						'label'       => false,
						'div'         => false,
					)) . PHP_EOL;
				?>
				</div><!-- /.input -->
				<div class="button"><?php
					echo $this->Form->submit('認証', array(
						'label'       => false,
						'div'         => false,
					)) . PHP_EOL;
				?></div><!-- /.button -->
				<div class="logo">
					<img src="<?php echo $this->Html->url('/img/common/logo2.png', true); ?>" width="85" height="20" />
				</div><!-- /.logo -->
			<?php echo $this->Form->end() . PHP_EOL; ?>
		</div><!-- /#container -->
	</div><!-- /#wrapper -->
