<?php
/**
 * View: Index
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '');

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<div id="container">
		<a href="<?php echo $this->Html->url("/course", true); ?>" id="course" class="applink">
			<div>
				<img src="<?php echo $this->Html->url("/img/common/btn-course.png", true); ?>" width="225" height="131" alt="コースメニュー">
			</div>
		</a><!-- /#course -->
		<a href="<?php echo $this->Html->url("/select", true); ?>" id="select" class="applink">
			<div>
				<img src="<?php echo $this->Html->url("/img/common/btn-select.png", true); ?>" width="225" height="131" alt="セレクトメニュー">
			</div>
		</a><!-- /#select -->
		<a href="<?php echo $this->Html->url('/unauth', true); ?>" class="unauth applink">×</a>
	</div><!-- /#container -->
