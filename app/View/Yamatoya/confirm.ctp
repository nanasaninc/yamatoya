<?php
/**
 * View: Confirm
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '選択内容確認画面');

	/**
	 * Folder class activated.
	 */
	App::uses('Folder', 'Utility');
	$folder_m = new Folder(APP . "webroot/files/menus/");
	$folder_p = new Folder(APP . "webroot/files/products/");

	/**
	 * Initialize total plice.
	 */
	$total = ($this->request->data['Session']['MenuType']['name_en'] == 'Select')? 11400: 0;

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<div id="wrapper">
		<div id="container">
			<div id="confirm">
				<header>
					<div class="inner">
						<?php
							echo ($this->request->data['Session']['MenuType']['name_en'] == 'Course')?
								"<div class=\"course-logo\"><img src=\"" . $this->Html->url('/img/common/btn-course.png', true) ."\"></div>" . PHP_EOL:
								"<div class=\"select-logo\"><img src=\"" . $this->Html->url('/img/common/btn-select.png', true) ."\"></div>" . PHP_EOL;
						?>
						<p><?php echo sp_dotw_jp($this->Time->format('Y年n月j日 l G:i', $this->request->data['Session']['Order']['datetime'])); ?>～
						<br><?php echo h($this->request->data['Session']['Order']['groom_name']); ?>家 <?php echo h($this->request->data['Session']['Order']['bride_name']); ?>家　ご両家　<?php
							echo h($this->request->data['Session']['MenuType']['name']);
							if ($this->request->data['Session']['MenuType']['name_en'] == 'Course')
								echo "&nbsp;{$this->request->data['Session']['Menu'][0]['MenuType']['name']}&nbsp;{$this->request->data['Session']['Menu'][0]['name']}&nbsp;({$this->request->data['Session']['Menu'][0]['kana']})";
						?></p>
					</div>
				</header>
				<div class="inner">
					<div id="menu-list">
						<table>
<?php foreach ($this->request->data['Session']['Menu'] as $menu): $total += ($this->request->data['Session']['MenuType']['name_en'] == 'Select')? $menu['difference_price']: $menu['price']; ?>
<?php foreach ($menu['MenuProduct'] as $product): ?>
							<tr class="thumbnail num0">
								<th style="width:110px;text-align:left">
									<div class="thumbnail"><?php
										$files = $folder_p->find("{$product['Product']['image']}@th.jpg");
										$image = !empty($files)?
											"/files/products/{$files[0]}":
											"/img/common/nowprinting-product@th.png";
										echo "<a href=\"#gallery\" class=\"popup\"><img src=\"" . $this->Html->url($image, true) . "\" width=\"70\" height=\"70\"></a>";
										$galleries[] = array(
											'name'      => $product['Product']['name'],
											'image'     => str_replace('@th', '' , $image),
											'type_name' => ($this->request->data['Session']['MenuType']['name_en'] == 'Select' && !empty($product['Product']['ProductType']))? $product['Product']['ProductType']['name']: null,
											'type_slug' => ($this->request->data['Session']['MenuType']['name_en'] == 'Select' && !empty($product['Product']['ProductType']))? $product['Product']['ProductType']['slug']: null,
											'price'     => $menu['difference_price'],
										);
									?></div>
								</th>
								<td class="text">
									<span><?php
										echo !empty($product['Product']['ProductType'])?
											"<span class=\"icon {$product['Product']['ProductType']['slug']}\">{$product['Product']['ProductType']['name']}</span>": null;
										echo h($product['Product']['name']);
									?></span>
									<span class="plus"><?php
										if ($menu['difference_price'] > 0)
											echo "(+" . $this->Number->currency($menu['difference_price']) . ")";
									?></span><!-- /.plus -->
									<br><?php echo h($product['Product']['description']) . PHP_EOL; ?>
								</td>
<?php if ($this->request->data['Session']['MenuType']['name_en'] == 'Select'): ?>
								<td class="button"><?php
									echo "<div class=\"change\"><a href=\"" . $this->Html->url("/select?p={$menu['MenuType']['id']}", true) . "\" class=\"applink\">変更</a></div>";
								?></td><!-- /.button -->
<?php endif; ?>
							</tr>
<?php endforeach; ?>
<?php endforeach; ?>
						</table>
					</div><!-- /#menu-list -->
					<div class="total">
						<p>一名様合計　<?php echo $this->Number->currency($total); ?>(税金・サービス料別)</p>
					</div><!-- /.total -->
					<ul class="buttons">
						<li class="button back2"><?php
							echo "<a href=\"" . $this->Html->url('/', true) . "\" class=\"applink\"><span>選び直す</span></a>";
						?></li><!-- /.button.back -->
						<li class="button back"><?php
							if (isset($this->request->query['back'])) {
								echo "<a href=\"" . $this->Html->url("/" . Inflector::underscore($this->request->query['back']), true) . "\" class=\"applink\"><span>戻る</span></a>";
							} else {
								echo ($this->request->data['Session']['MenuType']['name_en'] == 'Course')?
									"<a href=\"" . $this->Html->url("/" . Inflector::underscore($this->request->data['Session']['Menu'][0]['MenuType']['name_en']) . "?p={$this->request->data['Session']['Menu'][0]['id']}", true) . "\" class=\"applink\"><span>戻る</span></a>":
									"<a href=\"" . $this->Html->url('/select', true) . "\" class=\"applink\"><span>戻る</span></a>";
							}
						?></li><!-- /.button.back -->
						<li class="button select"><?php
							echo $this->Form->create('Order');
							echo $this->Form->submit('決定する', array(
								'div'   => false,
							));
							echo $this->Form->end();
						?></li><!-- /.button.select -->
					</ul><!-- /.buttons -->
				</div><!-- /.inner -->

				<div class="hide">
					<div id="gallery" class="gallery">
						<div class="gallery-wrap">
							<div class="viewport">
								<div class="flipsnap">
<?php foreach ($galleries as $image): ?>
									<div class="item">
<?php if (!empty($image['type_name'])): ?>
										<div class="icon <?php echo $image['type_slug']; ?>"><?php echo $image['type_name']; ?></div>
<?php endif; ?>
<?php if ($image['price'] > 0): ?>
										<span class="plus">
											<p>+<?php echo $this->Number->currency($image['price']); ?></p>
										</span>
<?php endif; ?>
										<div class="pic"><?php
											echo "<img src=\"" . $this->Html->url($image['image'], true) . "\" width=\"680\" height=\"680\">";
										?></div>
										<p><?php echo h($image['name']); ?></p>
									</div>
<?php endforeach; ?>
								</div>
							</div>
						</div><!-- /.gallery-wrap -->
					</div><!-- /#gallery -->
				</div><!-- /.hide -->

			</div><!-- /#confirm -->
		</div><!-- /#container -->
	</div><!-- /#wrapper -->
