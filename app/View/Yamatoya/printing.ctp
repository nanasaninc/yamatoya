<?php
/**
 * View: Printing
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Yamatoya
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '印刷画面');

	/**
	 * Folder class activated.
	 */
	App::uses('Folder', 'Utility');
	$folder_m = new Folder(APP . "webroot/files/menus/");
	$folder_p = new Folder(APP . "webroot/files/products/");

	/**
	 * Initialize total plice.
	 */
	$total = ($order['MenuType']['name_en'] == 'Select')? 11400: 0;

?><?php $this->append('css'); ?>
<link rel="stylesheet" href="<?php echo $this->Html->url('/css/printing.css', true); ?>">
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function(){
	$('#menu-list .menu:nth-child(3n)').each(function(){
		$(this).css({marginRight: '0'});
	});
	var $setElm = $('.content .inner #menu-list .menu dl dd');
	var cutFigure = '23'; // カットする文字数
	var afterTxt = ' …'; // 文字カット後に表示するテキスト

	$setElm.each(function(){
		var textLength = $(this).text().length;
		var textTrim = $(this).text().substr(0,(cutFigure))

		if(cutFigure < textLength) {
			$(this).html(textTrim + afterTxt).css({visibility:'visible'});
		} else if(cutFigure >= textLength) {
			$(this).css({visibility:'visible'});
		}
	});
});
</script>
<?php $this->end(); ?>

	<div class="content">
		<div class="inner">
			<div id="head">
				<p class="date_name"><?php echo $this->Time->format('Y年n月j日', $order['Order']['datetime']); ?>　<?php echo h($order['Order']['groom_name']); ?>家　<?php echo h($order['Order']['bride_name']); ?>家　様</p>
				<p><?php
					echo h($order['MenuType']['name']);
					if ($order['MenuType']['name_en'] == 'Course') {
						echo "&nbsp;{$order['Menu'][0]['MenuType']['name']}&nbsp;";
						echo "「";
						echo "<ruby>{$order['Menu'][0]['name']}<rt>{$order['Menu'][0]['kana']}</rt></ruby>";
						echo "」";
					}
				?></p>
			</div>
<?php if ($order['MenuType']['name_en'] != 'Select'): ?>
			<div id="menuBox">
				<div id="mainImg"><?php
					$files = $folder_m->find("{$order['Menu'][0]['image']}.jpg");
					echo !empty($files)?
						"<img src=\"" . $this->Html->url("/files/menus/{$files[0]}", true) . "\" width=\"360\" height=\"433\">":
						"<img src=\"" . $this->Html->url("/img/common/nowprinting-menu.png", true) . "\" width=\"360\" height=\"433\">";
				?></div>
				<ul id="menuList">
<?php foreach ($order['Menu'] as $menu): $total += $menu['price']; ?>
<?php foreach ($menu['MenuProduct'] as $product): ?>
					<li><?php
						if (!empty($product['Product']['ProductType']))
							echo "<span class=\"{$product['Product']['ProductType']['slug']}\">{$product['Product']['ProductType']['name']}</span>";
						echo $product['Product']['name_ruby'];
						$price_slag = $product['Product']['ProductType']['slug'];
					?></li>
<?php endforeach; ?>
<?php endforeach; ?>
				</ul>
			</div>
<?php else: $price_slag = 'sl'; ?>
			<div id="menu-list">
<?php $count = 0; ?>
<?php foreach ($order['Menu'] as $menu): $total += $menu['difference_price']; ?>
<?php foreach ($menu['MenuProduct'] as $product): ?>
				<div class="menu">
<?php if ($count < 9): ?>
					<div class="menu-image"><?php
						echo !empty($product['Product']['ProductType'])? '<span class="type ' . $product['Product']['ProductType']['slug'] . '">' . $product['Product']['ProductType']['name'] . '</span>': null;
						$files = $folder_p->find("{$product['Product']['image']}.jpg");
						$image = !empty($files)?
							"/files/products/{$files[0]}":
							"/img/common/nowprinting-product.png";
						echo "<img src=\"" . $this->Html->url($image, true) . "\" width=\"142\" height=\"142\">";
					?></div>
<?php endif; ?>
					<dl>
						<dt><?php
							if (!empty($menu['MenuType']))
								echo "<span class=\"menu-type " . strtolower($menu['MenuType']['name_en']) . "\">{$menu['MenuType']['name']}</span>";
						?></dt>
						<dd><?php
							echo $product['Product']['name_ruby'];
						?></dd>
					</dl>
				</div>
<?php $count++; ?>
<?php endforeach; ?>
<?php endforeach; ?>
				<br style="clear:both">
			</div>
<?php endif; ?>
			<div id="<?php echo $price_slag; ?>">
				<div id="price">
					<p>一名様合計　<?php echo $this->Number->currency($total); ?><span>(税金・サービス料別)</span></p>
				</div>
			</div>
			<p class="caution">※季節によって内容が変わる場合がございます。ご了承ください。</p>
			<p><img src="<?php echo $this->Html->url("/img/print/logo.png", true); ?>" width="90" height="21" /></p>
		</div>
	</div>

<?php foreach ($order['Menu'] as $menu): $total += $menu['price']; ?>
<?php foreach ($menu['MenuProduct'] as $product): ?>
	<div class="content single">
		<div class="inner">
			<div class="single-img"><?php
				echo ($order['MenuType']['name_en'] == 'Select' && !empty($product['Product']['ProductType']))? '<span class="type ' . $product['Product']['ProductType']['slug'] . '">' . $product['Product']['ProductType']['name'] . '</span>': null;
				$files = $folder_p->find("{$product['Product']['image']}@2x.jpg");
				$image = !empty($files)?
					"/files/products/{$files[0]}":
					"/img/common/nowprinting-product@2x.png";
				echo "<img src=\"" . $this->Html->url($image, true) . "\" width=\"529\" height=\"529\">";
			?></div>
			<dl>
<?php if ($order['MenuType']['name_en'] != 'Select'): ?>
				<dt<?php echo !empty($product['Product']['ProductType'])? " class=\"{$product['Product']['ProductType']['slug']}\"": null; ?>><?php echo (!empty($product['Product']['ProductType']))? "{$product['Product']['ProductType']['name']}": "&nbsp;"; ?></dt>
<?php else: ?>
				<dt<?php echo !empty($menu['MenuType'])? " class=\"menu-type " . strtolower($menu['MenuType']['name_en']) . "\"": null; ?>><?php echo (!empty($menu['MenuType']))? "{$menu['MenuType']['name']}": "&nbsp;"; ?></dt>
<?php endif; ?>
				<dd><?php echo $product['Product']['name_ruby']; ?></dd>
			</dl>
		</div>
	</div>
<?php endforeach; ?>
<?php endforeach; ?>
