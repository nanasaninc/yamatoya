<?php
/**
 * View: Cpanel Login
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Cpanel
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '管理ログイン');

?><?php $this->append('css'); ?>
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/signin.css', true); ?>">
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>

	<?php echo $this->Form->create('User', array('class' => 'form-signin')) . PHP_EOL; ?>
		<h1 class="form-signin-heading text-center"><i class="fa fa-tachometer"></i></h1>
		<?php echo $this->Session->flash() . PHP_EOL; ?>
		<?php
			echo $this->Form->input('User.login_id', array(
				'type'        => 'text',
				'placeholder' => 'ログインID',
				'autofocus'   => true,
				'tabindex'    => 1,
				'class'       => 'form-control',
				'label'       => false,
				'div'         => false,
			)) . PHP_EOL;
		?>
		<?php
			echo $this->Form->input('User.password', array(
				'type'        => 'password',
				'placeholder' => 'パスワード',
				'tabindex'    => 1,
				'class'       => 'form-control',
				'label'       => false,
				'div'         => false,
			)) . PHP_EOL;
		?>
		<?php
			echo $this->Form->button('<i class="fa fa-sign-in"></i>&nbsp;ログイン', array(
				'type'        => 'submit',
				'class'       => 'btn btn-lg btn-primary btn-block',
				'tabindex'    => 1,
				'escape'      => false,
			)) . PHP_EOL;
		?>
	<?php echo $this->Form->end() . PHP_EOL; ?>
<?php
	echo isset($password)? $password: null;