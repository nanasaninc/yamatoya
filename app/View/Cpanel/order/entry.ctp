<?php
/**
 * View: Cpanel Order - Entry
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Cpanel.order
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '新規予約作成');

?><?php $this->append('css'); ?>
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/datepicker.css', true); ?>">
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function() {
	// Datepicker
	$('.datepicker').datepicker();
	// Error
	$.each($('.form-error'), function() {
		$(this).parents('.form-group').addClass('has-error').addClass('error');
	});
	// Alpha Numeric
	$('.alpha-numeric').change(function() {
		CommonFn.alphaNumeric(this);
	});
});
</script>
<?php $this->end(); ?>
<?php $this->append('sidebar'); ?>
<?php echo $this->element('sidebar/order') . PHP_EOL; ?>
<?php $this->end(); ?>

	<header>
		<h2><i class="fa fa-plus"></i>&nbsp;新規予約作成</h2>
		<?php echo $this->Session->flash() . PHP_EOL; ?>
	</header>

	<section class="row">
		<div class="col-md-12">
			<?php echo $this->Form->create('Order') . PHP_EOL; ?>
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_list", true); ?>" class="btn btn-default btn-sm pull-left"><i class="fa fa-undo"></i>&nbsp;キャンセル</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><?php
						echo $this->Form->button('<i class="fa fa-check"></i>&nbsp;登録', array(
							'type'        => 'submit',
							'tabindex'    => 1,
							'class'       => 'btn btn-success btn-sm pull-right',
							'div'         => false,
							'escape'      => false,
						));
					?></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
			<h3><small><i class="fa fa-user"></i>&nbsp;予約情報</small></h3>
			<table class="table table-bordered">
				<tbody>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.order_name', '予約ID', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.order_name', array(
								'type'        => 'text',
								'placeholder' => '予約ID',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.order_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.groom_name', '新郎氏名', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.groom_name', array(
								'type'        => 'text',
								'placeholder' => '新郎氏名',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.groom_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.bride_name', '新婦氏名', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.bride_name', array(
								'type'        => 'text',
								'placeholder' => '新婦氏名',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.bride_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.date', 'ご予約日時', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.date', array(
								'type'        => 'text',
								'placeholder' => 'ご予約日',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm datepicker',
								'div'         => false,
								'between'     => '<div class="col col-md-3">',
								'wrapInput'   => 'input-group',
								'afterInput'  => $this->Form->label('Order.date', '<i class="fa fa-calendar"></i>', array('class' => 'input-group-addon')),
								'after'       => '</div>',
							));
							echo $this->Form->input('Order.hour', array(
								'type'        => 'select',
								'empty'       => false,
								'options'     => array('00' => '00', '01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23'),
								'default'     => '10',
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-xs-2 time">',
								'after'       => '</div>',
							));
							echo '<span class="separator">：</span>';
							echo $this->Form->input('Order.minute', array(
								'type'        => 'select',
								'empty'       => false,
								'options'     => array('00' => '00', '15' => '15', '30' => '30', '45' => '45'),
								'default'     => '00',
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-xs-2 time">',
								'after'       => '</div>',
							));
							echo '<span class="separator">披露宴</span>';
							echo $this->Form->error('Order.date', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.comment1', '備考1', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.comment1', array(
								'type'        => 'textarea',
								'rows'        => 6,
								'placeholder' => '備考1',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-6">',
								'after'       => '</div>',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.comment2', '備考2', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.comment2', array(
								'type'        => 'textarea',
								'rows'        => 6,
								'placeholder' => '備考2',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-6">',
								'after'       => '</div>',
							));
						?></td>
					</tr>
				</tbody>
			</table>
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_list", true); ?>" class="btn btn-default btn-sm pull-left"><i class="fa fa-undo"></i>&nbsp;キャンセル</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><?php
						echo $this->Form->button('<i class="fa fa-check"></i>&nbsp;登録', array(
							'type'        => 'submit',
							'tabindex'    => 1,
							'class'       => 'btn btn-success btn-sm pull-right',
							'div'         => false,
							'escape'      => false,
						));
					?></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
			<?php echo $this->Form->end() . PHP_EOL; ?>
		</div><!-- /.col-md-12 -->
	</section><!-- /.row -->
