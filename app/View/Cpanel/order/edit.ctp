<?php
/**
 * View: Cpanel Order - Edit
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Cpanel.order
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '予約情報編集');

?><?php $this->append('css'); ?>
<style>
.select-label {
	font-weight: normal;
}
.select-label .radio {
	margin-top: 0;
	margin-bottom: 0;
	min-height: initial;
}
.select-label .radio input[type="radio"] {
	margin-top: 8px;
}
.select-label input[type="radio"],
.select-label input[type="checkbox"] {
	margin-right: 10px;
}
#select ol {
	margin: 0;
	padding-left: 10px;
	list-style-type: none;
}
</style>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function() {
	// Datepicker
	$('.datepicker').datepicker();
	// Error
	$.each($('.form-error'), function() {
		$(this).parents('.form-group').addClass('has-error').addClass('error');
	});
	// Alpha Numeric
	$('.alpha-numeric').change(function() {
		CommonFn.alphaNumeric(this);
	});
	// Change Event
	CommonFn.menuSwitch();
	$('#OrderMenuTypeId').change(function() {
		CommonFn.menuSwitch();
		$('.products').addClass('hide');
		CommonFn.courseSwitch();
	});
	$('.course-select').change(function() {
		CommonFn.courseSwitch();
	});

});
// form debug
$(function() {
	$('input').removeAttr('required');
});
// Validate alert.
$(function() {
	$('form').submit(function() {
		var radio = $('<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#">×</a>選択されていない項目があります。</div>'),
			check = $('<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#">×</a>選択上限を超えた項目があります。</div>');
		if ($('#OrderMenuTypeId').val() == '5') {
<?php $i = 0; foreach ($pulldown['Menu']['Select'] as $type_name => $menus): if (!$pulldown['SelectM'][$type_name]): ?>
			if ($("[id^=Menu<?php echo $i; ?>OrderMenuMenuId]:checked").val() == undefined) {
				$('#message').html(radio);
				return false;
			}
<?php else: ?>
			if ($(".check<?php echo $pulldown['SelectN'][$type_name]; ?>:checked").length > 3) {
				$('#message').html(check);
				return false;
			}
<?php endif; $i++; endforeach; ?>
		}
	});
});
</script>
<?php $this->end(); ?>
<?php $this->append('sidebar'); ?>
<?php echo $this->element('sidebar/order') . PHP_EOL; ?>
<?php $this->end(); ?>

	<header>
		<h2><i class="fa fa-edit"></i>&nbsp;予約情報編集</h2>
		<?php echo $this->Session->flash() . PHP_EOL; ?>
	</header>

	<section class="row">
		<div class="col-md-12">
			<?php echo $this->Form->create('Order') . PHP_EOL; ?>
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_detail/{$this->request->data['Order']['id']}", true); ?>" class="btn btn-default btn-sm pull-left"><i class="fa fa-undo"></i>&nbsp;詳細に戻る</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><?php
						echo $this->Form->button('<i class="fa fa-check"></i>&nbsp;更新', array(
							'type'        => 'submit',
							'tabindex'    => 1,
							'class'       => 'btn btn-success btn-sm pull-right',
							'div'         => false,
							'escape'      => false,
						));
					?></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
			<h3><small><i class="fa fa-user"></i>&nbsp;予約情報</small></h3>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th class="col col-md-2">ID</th>
						<td class="col col-md-10"><?php
							echo h($this->request->data['Order']['id']);
							echo $this->Form->hidden('Order.id');
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.order_name', '予約ID', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.order_name', array(
								'type'        => 'text',
								'placeholder' => '予約ID',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm alpha-numeric',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.order_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.groom_name', '新郎氏名', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.groom_name', array(
								'type'        => 'text',
								'placeholder' => '新郎氏名',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.groom_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.bride_name', '新婦氏名', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.bride_name', array(
								'type'        => 'text',
								'placeholder' => '新婦氏名',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-4">',
								'after'       => '</div>',
							));
							echo $this->Form->error('Order.bride_name', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.date', 'ご予約日時', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.date', array(
								'type'        => 'text',
								'placeholder' => 'ご予約日',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm datepicker',
								'div'         => false,
								'between'     => '<div class="col col-md-3">',
								'wrapInput'   => 'input-group',
								'afterInput'  => $this->Form->label('Order.date', '<i class="fa fa-calendar"></i>', array('class' => 'input-group-addon')),
								'after'       => '</div>',
							));
							echo $this->Form->input('Order.hour', array(
								'type'        => 'select',
								'empty'       => false,
								'options'     => array('00' => '00', '01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23'),
								'default'     => '10',
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-xs-2 time">',
								'after'       => '</div>',
							));
							echo '<span class="separator">：</span>';
							echo $this->Form->input('Order.minute', array(
								'type'        => 'select',
								'empty'       => false,
								'options'     => array('00' => '00', '15' => '15', '30' => '30', '45' => '45'),
								'default'     => '00',
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-xs-2 time">',
								'after'       => '</div>',
							));
							echo '<span class="separator">披露宴</span>';
							echo $this->Form->error('Order.date', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.comment1', '備考1', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.comment1', array(
								'type'        => 'textarea',
								'rows'        => 6,
								'placeholder' => '備考1',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-6">',
								'after'       => '</div>',
							));
						?></td>
					</tr>
					<tr class="form-group">
						<th class="col-md-2"><?php
							echo $this->Form->label('Order.comment2', '備考2', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col-md-10"><?php
							echo $this->Form->input('Order.comment2', array(
								'type'        => 'textarea',
								'rows'        => 6,
								'placeholder' => '備考2',
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-md-6">',
								'after'       => '</div>',
							));
						?></td>
					</tr>
				</tbody>
			</table><!-- /.table -->
			<h3><small><i class="fa fa-book"></i>&nbsp;メニュー情報</small></h3>
			<table class="table table-bordered">
				<tbody>
					<tr class="form-group">
						<th class="col col-md-2"><?php
							echo $this->Form->label('Order.menu_type_id', '種類', array(
								'class'       => 'control-label',
							));
						?></th>
						<td class="col col-md-10"><?php
							echo $this->Form->input('Order.menu_type_id', array(
								'type'        => 'select',
								'empty'       => '未選択',
								'options'     => $pulldown['MenuType'],
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								'between'     => '<div class="col col-xs-3">',
								'after'       => '</div>',
							));
						?></td>
					</tr>
				</tbody>
				<tbody id="course">
					<tr class="form-group">
						<th><?php
							echo $this->Form->label('Menu.0.OrderMenu.menu_id', 'コース', array(
								'class'       => 'control-label',
							));
						?></th>
						<td><?php
							echo $this->Form->input('Menu.0.OrderMenu.menu_id', array(
								'type'        => 'select',
								'empty'       => '未選択',
								'options'     => $pulldown['Menu']['Course'],
								'escape'      => false,
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'error'       => false,
								'class'       => 'form-control input-sm course-select',
								'div'         => false,
								'between'     => '<div class="col col-xs-5">',
								'after'       => '</div>',
							));
							echo $this->Form->error('OrderMenu.menu_id.0', null, array(
								'class'       => 'text-danger error',
								'wrap'        => 'p',
							));
						?></td>
					</tr>
<?php foreach ($pulldown['Course'] as $course): ?>
					<tr class="products<?php echo !empty($this->request->data['Menu'])? (($this->request->data['Menu'][0]['OrderMenu']['menu_id'] != $course['Menu']['id'])? ' hide': null): ' hide'; ?>" id="course<?php echo h($course['Menu']['id']); ?>">
						<th>内容</th>
						<td>
							<ul>
<?php foreach ($course['MenuProduct'] as $product): ?>
								<li><span class="<?php echo $product['Product']['ProductType']['slug']; ?>"><?php
									echo $product['Product']['ProductType']['name'];
								?></span><?php
									echo h($product['Product']['name']);
								?></li>
<?php endforeach; ?>
							</ul>
						</td>
					</tr>
<?php endforeach; ?>
				</tbody><!-- /#course -->
				<tbody id="select">
<?php $count = $num = 0; foreach ($pulldown['Menu']['Select'] as $type_name => $menus): ?>
					<tr class="form-group">
						<th><?php echo $type_name; ?></th>
						<td>
							<ol>
<?php foreach ($menus as $menu_id => $menu_name): ?>
								<li><label class="select-label"><?php
									$product_type = !empty($pulldown['Select'][$menu_id]['MenuProduct'][0]['Product']['ProductType'])?
										"{$pulldown['Select'][$menu_id]['MenuProduct'][0]['Product']['ProductType']['name']}／":
										null;
									$difference_price = ($pulldown['Select'][$menu_id]['Menu']['difference_price'] > 0)?
										' (+' . $this->Number->currency($pulldown['Select'][$menu_id]['Menu']['difference_price']) . ')':
										null;
									if (!$pulldown['SelectM'][$type_name] && !$pulldown['SelectR'][$menu_id]) {
										$options = array(
											'type'        => 'radio',
											'options'     => array($menu_id => "{$product_type}{$menu_name}{$difference_price}"),
											'hiddenField' => false,
											'class'       => "radio{$pulldown['SelectN'][$type_name]}",
											'label'       => false,
											'legend'      => false,
											'div'         => false,
										);
										if (array_search($menu_id, $order['Order']['menus']) !== false) {
											$options['value'] = $menu_id;
										}
										echo $this->Form->input("Menu.{$num}.OrderMenu.menu_id", $options);
									} else {
										if ($pulldown['SelectR'][$menu_id])
											$num++;
										$options = array(
											'value'       => $menu_id,
											'hiddenField' => false,
											'class'       => "check{$pulldown['SelectN'][$type_name]}",
											'label'       => false,
											'div'         => false,
											'checked'     => (boolean)$pulldown['SelectR'][$menu_id],
										);
										if ((boolean)$pulldown['SelectR'][$menu_id]) {
											// Required menu.
											$options['checked'] = true;
											// $options['onclick'] = 'return false;';
											// $options['class']   = 'hide';
										} else {
											// Check selected menu.
											if (array_search($menu_id, $order['Order']['menus']) !== false) {
												$options['checked'] = true;
											}
										}
										echo $this->Form->checkbox("Menu.{$num}.OrderMenu.menu_id", $options);
										echo "{$product_type}{$menu_name}{$difference_price}";
										$num++;
									}
								?></label></li>
<?php endforeach; ?>
							</ol>
						</td>
					</tr>
<?php $count++; $num++; endforeach; ?>
				</tbody><!-- /#select -->
			</table><!-- /.table -->
			<div id="message"></div>
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_detail/{$this->request->data['Order']['id']}", true); ?>" class="btn btn-default btn-sm pull-left"><i class="fa fa-undo"></i>&nbsp;詳細に戻る</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><?php
						echo $this->Form->button('<i class="fa fa-check"></i>&nbsp;更新', array(
							'type'        => 'submit',
							'tabindex'    => 1,
							'class'       => 'btn btn-success btn-sm pull-right',
							'div'         => false,
							'escape'      => false,
						));
					?></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
			<?php echo $this->Form->end() . PHP_EOL; ?>
		</div><!-- /.col-md-12 -->
	</section><!-- /.row -->
