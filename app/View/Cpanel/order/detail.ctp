<?php
/**
 * View: Cpanel Order - Detail
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Cpanel.order
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '予約情報詳細');

?><?php $this->append('css'); ?>
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<?php $this->end(); ?>
<?php $this->append('sidebar'); ?>
<?php echo $this->element('sidebar/order') . PHP_EOL; ?>
<?php $this->end(); ?>

	<header>
		<h2><i class="fa fa-info-circle"></i>&nbsp;予約情報詳細</h2>
		<?php echo $this->Session->flash() . PHP_EOL; ?>
	</header>

	<section class="row">
		<div class="col-md-12">
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_list", true); ?>" class="btn btn-default btn-sm"><i class="fa fa-undo"></i>&nbsp;一覧に戻る</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_edit/{$order['Order']['id']}", true); ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp;編集</a></li>
					<li><a href="<?php echo $this->Html->url("/printing?id={$order['Order']['id']}", true); ?>" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-print"></i>&nbsp;印刷</a></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
			<h3><small><i class="fa fa-user"></i>&nbsp;予約情報</small></h3>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th class="col col-md-2">ID</th>
						<td class="col col-md-10"><?php
							echo $order['Order']['id'];
						?></td>
					</tr>
					<tr>
						<th>予約ID</th>
						<td><?php
							echo $order['Order']['order_name'];
						?></td>
					</tr>
					<tr>
						<th>新郎新婦</th>
						<td><?php
							echo $order['Order']['groom_name'] . '&nbsp;家';
						?>&nbsp;・&nbsp;<?php
							echo $order['Order']['bride_name'] . '&nbsp;家';
						?></td>
					</tr>
					<tr>
						<th>予約日</th>
						<td><?php
							echo $this->Time->format('Y年m月d日 H時i分 披露宴', $order['Order']['datetime']);
						?></td>
					</tr>
					<tr>
						<th>備考1</th>
						<td><?php
							echo !empty($order['Order']['comment1'])? n2b_h($order['Order']['comment1']): '-';
						?></td>
					</tr>
					<tr>
						<th>備考2</th>
						<td><?php
							echo !empty($order['Order']['comment2'])? n2b_h($order['Order']['comment2']): '-';
						?></td>
					</tr>
				</tbody>
			</table><!-- /.table -->
			<h3><small><i class="fa fa-book"></i>&nbsp;メニュー情報</small></h3>
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th class="col col-md-2">種類</th>
						<td class="col col-md-10"><?php
							echo is_null($order['MenuType']['name'])?
								'未選択':
								h($order['MenuType']['name']);
						?></td>
					</tr>
<?php if ($order['Order']['menu_type_id'] == 1): ?>
					<tr>
						<th><?php
							echo $order['Menu'][0]['name'];
						?><br><small>(<?php
							echo $order['Menu'][0]['kana'];
						?>)</small></th>
						<td class="products">
							<strong></strong>
							<ul><?php
								foreach ($order['Menu'][0]['MenuProduct'] as $key => $product):
							?><li><span class="<?php echo $product['Product']['ProductType']['slug']; ?>"><?php
								echo $product['Product']['ProductType']['name'];
							?></span><?php
								echo $product['Product']['name'];
							?></li><?php
								endforeach;
							?></ul>
						</td>
					</tr>
<?php elseif ($order['Order']['menu_type_id'] == 5): ?>
<?php foreach ($order['Menu'] as $key => $menu): ?>
					<tr>
						<th><?php
							echo $menu['MenuType']['name'];
						?></th>
						<td><?php
							echo $menu['MenuProduct'][0]['Product']['name'];
						?></td>
					</tr>
<?php endforeach; ?>
<?php endif; ?>
				</tbody>
			</table><!-- /.table -->
			<div class="well well-sm clearfix">
				<ul class="btn-menu pull-left">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_list", true); ?>" class="btn btn-default btn-sm"><i class="fa fa-undo"></i>&nbsp;一覧に戻る</a></li>
				</ul><!-- /.btn-menu -->
				<ul class="btn-menu pull-right">
					<li><a href="<?php echo $this->Html->url("/admin/cpanel/order_edit/{$order['Order']['id']}", true); ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>&nbsp;編集</a></li>
					<li><a href="<?php echo $this->Html->url("/printing?id={$order['Order']['id']}", true); ?>" target="_blank" class="btn btn-warning btn-sm"><i class="fa fa-print"></i>&nbsp;印刷</a></li>
				</ul><!-- /.btn-menu -->
			</div><!-- /.well -->
		</div><!-- /.col-md-12 -->
	</section><!-- /.row -->
