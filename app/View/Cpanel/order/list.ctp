<?php
/**
 * View: Cpanel Order - List
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Cpanel.order
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the page name.
	 */
	$this->assign('page-title', '予約一覧');

?><?php $this->append('css'); ?>
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/datepicker.css', true); ?>">
<?php $this->end(); ?>
<?php $this->append('script'); ?>
<script>
$(function() {
	$('.datepicker').datepicker();
});
</script>
<?php $this->end(); ?>
<?php $this->append('sidebar'); ?>
<?php echo $this->element('sidebar/order') . PHP_EOL; ?>
<?php $this->end(); ?>

	<header>
		<h2><i class="fa fa-list"></i>&nbsp;予約一覧&nbsp;<a href="<?php echo $this->Html->url("/admin/cpanel/order_entry", true); ?>" class="btn btn-default btn-xs"><i class="fa fa-plus"></i>&nbsp;新規予約作成</a></h2>
		<?php echo $this->Session->flash() . PHP_EOL; ?>
	</header>

	<section class="row">
		<div class="col col-md-12">
			<?php echo $this->Form->create('Order', array('class' => 'well well-sm')) . PHP_EOL; ?>
				<fieldset class="col col-md-12">
					<h3 style="margin:0"><small><i class="fa fa-search"></i>&nbsp;検索</small></h3>
					<dl class="form-group dl-horizontal">
						<dt><?php
							echo $this->Form->label('Order.order_name', '予約ID', array(
								'class'       => 'control-label',
							));
						?></dt>
						<dd><?php
							echo $this->Form->input('Order.order_name', array(
								'type'        => 'text',
								'placeholder' => '予約ID',
								'tabindex'    => 1,
								'label'       => false,
								'required'    => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								// 'between'     => '<div class="col col-md-12">',
								// 'after'       => '</div>',
							));
						?></dd>
					</dl>
					<dl class="form-group dl-horizontal">
						<dt><?php
							echo $this->Form->label('Order.keyword', 'お客様名', array(
								'class'       => 'control-label',
							));
						?></dt>
						<dd><?php
							echo $this->Form->input('Order.keyword', array(
								'type'        => 'text',
								'placeholder' => 'お客様名',
								'tabindex'    => 1,
								'label'       => false,
								'required'    => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								// 'between'     => '<div class="col col-md-12">',
								// 'after'       => '</div>',
							));
							echo $this->Form->hidden('Order.field', array('value' => 'groom_name,bride_name'));
						?></dd>
					</dl>
					<dl class="form-group dl-horizontal">
						<dt><?php
							echo $this->Form->label('Order.menu_type_id', 'タイプ', array(
								'class'       => 'control-label',
							));
						?></dt>
						<dd><?php
							echo $this->Form->input('Order.menu_type_id', array(
								'type'        => 'select',
								'empty'       => '未選択',
								'options'     => $pulldown['MenuType'],
								'showParents' => true,
								'tabindex'    => 1,
								'label'       => false,
								'required'    => false,
								'class'       => 'form-control input-sm',
								'div'         => false,
								// 'between'     => '<div class="col col-md-12">',
								// 'after'       => '</div>',
							));
						?></dd>
					</dl>
					<dl class="form-group dl-horizontal">
						<dt><?php
							echo $this->Form->label('Order.date_from', 'ご予約日', array(
								'class'       => 'control-label',
							));
						?></dt>
						<dd><?php
							echo $this->Form->input('Order.date_from', array(
								'type'        => 'text',
								'placeholder' => 'ご予約日',
								'tabindex'    => 1,
								'label'       => false,
								'required'    => false,
								'class'       => 'form-control input-sm datepicker',
								'div'         => false,
								'between'     => '<div class="col col-md-3" style="padding:0">',
								'wrapInput'   => 'input-group',
								'afterInput'  => $this->Form->label('Order.date_from', '<i class="fa fa-calendar"></i>', array('class' => 'input-group-addon')),
								'after'       => '</div>',
							));
							echo '<span class="separator">〜</span>';
							echo $this->Form->input('Order.date_to', array(
								'type'        => 'text',
								'placeholder' => 'ご予約日',
								'tabindex'    => 1,
								'label'       => false,
								'required'    => false,
								'class'       => 'form-control input-sm datepicker',
								'div'         => false,
								'between'     => '<div class="col col-md-3" style="padding:0">',
								'wrapInput'   => 'input-group',
								'afterInput'  => $this->Form->label('Order.date_to', '<i class="fa fa-calendar"></i>', array('class' => 'input-group-addon')),
								'after'       => '</div>',
							));
						?></dd>
					</dl>
					<div class="form-group"><?php
						echo $this->Form->button('<i class="fa fa-search"></i>&nbsp;検索', array(
							'type'        => 'submit',
							'tabindex'    => 1,
							'class'       => 'btn btn-default btn-sm btn-block',
							'div'         => false,
							'escape'      => false,
						));
					?></div>
				</fieldset>
			<?php echo $this->Form->end() . PHP_EOL; ?>
			<h3><small><i class="fa fa-list"></i>&nbsp;予約一覧</small></h3>
			<table class="table table-bordered table-hover" style="margin-bottom:2px">
				<thead>
					<tr>
						<th class="col-md-1 text-center"><?php
							echo $this->Paginator->sort('Order.id', 'ID');
						?></th>
						<th class="col-md-3 text-center"><?php
							echo $this->Paginator->sort('Order.order_name', '予約ID');
						?></th>
						<th class="col-md-3 text-center"><?php
							echo $this->Paginator->sort('Order.date', 'ご予約日時');
						?></th>
						<th class="col-md-2 text-center"><?php
							echo $this->Paginator->sort('Order.groom_name', '新郎');
						?>&nbsp;/&nbsp;<?php
							echo $this->Paginator->sort('Order.bride_name', '新婦');
						?></th>
						<th class="col-md-2 text-center"><?php
							echo $this->Paginator->sort('Order.menu_type_id', 'タイプ');
						?></th>
						<th class="col-md-1 text-center">&nbsp;</th>
					</tr>
				</thead>
				<tbody>
<?php if (!empty($orders)): foreach ($orders as $key => $order): ?>
					<tr>
						<td class="text-center"><?php
							echo h($order['Order']['id']);
						?></td>
						<td><?php
							echo h($order['Order']['order_name']);
						?></td>
						<td><?php
							echo $this->Time->format('Y年m月d日 H時i分', $order['Order']['datetime']);
						?></td>
						<td><?php
							echo h($order['Order']['groom_name']) . '&nbsp;家';
						?>&nbsp;・&nbsp;<?php
							echo h($order['Order']['bride_name']) . '&nbsp;家';
						?></td>
						<td><?php
							echo is_null($order['Order']['menu_type_id'])?
								'未選択':
								h($order['MenuType']['name']);
						?></td>
						<td class="text-center">
							<a href="<?php echo $this->Html->url("/admin/cpanel/order_detail/{$order['Order']['id']}", true); ?>" class="btn btn-info btn-sm btn-block"><i class="fa fa-info-circle"></i>&nbsp;詳細</a>
							<a href="<?php echo $this->Html->url("/admin/cpanel/order_edit/{$order['Order']['id']}", true); ?>" class="btn btn-primary btn-sm btn-block"><i class="fa fa-edit"></i>&nbsp;編集</a>
						</td>
					</tr>
<?php endforeach; else: ?>
					<tr>
						<td class="text-center text-danger"><i class="fa fa-meh-o"></i></td>
						<td colspan="5">現在参照可能な予約情報はありません。</td>
					</tr>
<?php endif; ?>
				</tbody>
			</table>
			<nav class="clearfix">
				<nav><span class="pagination-counter pull-left"><?php
					echo $this->Paginator->counter(array(
						'format' => __('{:page} / {:pages} ページ ({:count}件中 {:start}件〜{:end}件)'),
					));
				?></span><?php
					echo $this->Paginator->pagination(array(
						'ul' => 'pagination pagination-sm pull-right',
					));
				?></nav>
			</nav>
		</div><!-- /.col-md-12 -->
	</section><!-- /.row -->
