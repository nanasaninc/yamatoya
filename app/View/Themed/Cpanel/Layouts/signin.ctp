<?php
/**
 * Layout frame - Sign-In
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Layouts
 * @since       yamatoya.sys v 1.0
 */
?><!DOCTYPE html>
<html lang="ja">
<?php echo $this->element('head') . PHP_EOL; ?>
<body>

<div class="container">
<?php echo $this->fetch('content') . PHP_EOL; ?>
</div><!-- /.container -->

<?php echo $this->element('foot') . PHP_EOL; ?>
</body>
</html>