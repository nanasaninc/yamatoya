<?php
/**
 * Layout frame - Default
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Layouts
 * @since       yamatoya.sys v 1.0
 */
?><!DOCTYPE html>
<html lang="ja">
<?php echo $this->element('head') . PHP_EOL; ?>
<body data-spy="scroll" data-target=".subnav" data-offset="80">

<?php echo $this->element('navbar') . PHP_EOL; ?>

<div class="wrapper">

<nav class="sidebar-wrapper">
<?php echo $this->fetch('sidebar') . PHP_EOL; ?>
</nav><!-- /.sidebar-wrapper -->

<div class="content-wrapper">

<div class="container">
<div class="sidebar-minimized"><span class="sidebar-toggle"><i class="fa fa-arrow-right"></i></span></div>
<?php echo $this->fetch('content') . PHP_EOL; ?>
</div><!-- /.container -->

<footer class="footer">
<div class="container">
	<?php if (!CakePlugin::loaded('DebugKit')) echo $this->element('Errors/sql_dump') . PHP_EOL; ?>
	<p class="copy"><?php echo Configure::read('Setting.copyright'); ?></p>
</div><!-- /.container -->
</footer><!-- /.footer -->

</div><!-- /.content-wrapper -->

</div><!-- /.wrapper -->

<?php echo $this->element('foot') . PHP_EOL; ?>
</body>
</html>