<?php
/**
 * Element block - Head
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * Check options.
	 */
	if (!isset($class)) $class = false;
	if (!isset($close)) $close = true;

?>
<div class="alert<?php echo ($class)? ' alert-' . $class: null; ?>">
<?php if ($close): ?>
	<a class="close" data-dismiss="alert" href="#">×</a>
<?php endif; ?>
	<?php echo $message; ?>
</div>