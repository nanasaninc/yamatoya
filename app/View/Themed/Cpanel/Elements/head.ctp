<?php
/**
 * Element block - Head
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements
 * @since       yamatoya.sys v 1.0
 */
	/**
	 * To set the <title>.
	 */
	$page_title = $this->fetch('page-title');
	if (empty($page_title))
		$this->assign('title', Configure::read('Setting.site-name') . '&nbsp;&raquo;&nbsp;' . Configure::read('Setting.description'));
	else
		$this->assign('title', $page_title . '&nbsp;&raquo;&nbsp;' . Configure::read('Setting.site-name'));

?><head>
<meta charset="UTF-8">
<title><?php echo $this->fetch('title'); ?></title>
<meta name="description" content="<?php echo Configure::read('Setting.description'); ?>">
<meta name="keywords" content="<?php echo Configure::read('Setting.keywords'); ?>">
<?php echo $this->fetch('meta'); ?>

<!-- Styles -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no">
<meta name="format-detection" content="telephone=no">
<meta name="robots" content="noindex,nofollow">
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/bootstrap.css', true); ?>">
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/sidebar.css', true); ?>">
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/font-awesome.css', true); ?>">
<link rel="stylesheet" href="<?php echo $this->Html->url('/theme/Cpanel/css/style.css', true); ?>">
<?php echo $this->fetch('css'); ?>
<?php if (Configure::read('debug') > 0): ?>
<link rel="stylesheet" href="<?php echo $this->Html->url('/css/cake.debug.css', true); ?>">
<?php endif; ?>

<!-- Libs -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<!-- IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]><script type="text/javascript" src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script type="text/javascript" src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->

<!-- Touch icons -->
<link rel="shortcut icon" href="<?php echo $this->Html->url('/favicon.ico', true); ?>">

<!-- Application window -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-capable" content="yes">
</head>