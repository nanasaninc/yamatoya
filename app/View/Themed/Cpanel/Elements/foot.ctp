<?php
/**
 * Element block - Foot
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements
 * @since       yamatoya.sys v 1.0
 */
?><!-- Modals -->
<div class="modal fade" id="logoutModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">ログアウト</h4>
			</div><!-- /.modal-header -->
			<div class="modal-body">
				<p>ログアウトしますか？</p>
			</div><!-- /.modal-body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;キャンセル</button>
				<a class="btn btn-primary" href="<?php echo $this->Html->url('/admin/cpanel/logout', true); ?>"><i class="fa fa-sign-out"></i>&nbsp;ログアウト</a>
			</div><!-- /.modal-footer -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /#logoutModal -->
<?php echo $this->fetch('modal'); ?>

<!-- Scripts -->
<script src="<?php echo $this->Html->url('/theme/Cpanel/js/bootstrap.js', true); ?>"></script>
<script src="<?php echo $this->Html->url('/theme/Cpanel/js/sidebar.js', true); ?>"></script>
<script src="<?php echo $this->Html->url('/theme/Cpanel/js/ah-placeholder.js', true); ?>"></script>
<script src="<?php echo $this->Html->url('/theme/Cpanel/js/common.js', true); ?>"></script>
<?php echo $this->fetch('script'); ?>