<?php
/**
 * Element block - NavBar
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements
 * @since       yamatoya.sys v 1.0
 */
?><header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="<?php echo $this->Html->url("/admin/cpanel", true); ?>" class="navbar-brand"><?php echo Configure::read('Setting.site-name'); ?></a>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
			<ul class="nav navbar-nav navbar-user">
				<li class="dropdown">
					<a href="#" id="dropdown-user" role="button" class="dropdown-toggle" data-toggle="dropdown" title="ログイン情報"><i class="fa fa-user"></i>&nbsp;<b class="caret"></b></a>
					<ul class="dropdown-menu" aria-labelledby="dropdown-user">
						<li class="user-info">
							<img src="<?php echo $this->Html->url("/files/user/default.jpg"); ?>" alt="<?php echo h($administrator['name']); ?>" class="user-icon">
							<ul class="user-data">
								<li class="user-name"><i class="fa fa-user"></i>&nbsp;<?php echo h($administrator['name']); ?></li>
								<li><a href="#logoutModal" class="btn btn-default btn-xs" data-toggle="modal"><i class="fa fa-sign-out"></i>&nbsp;ログアウト</a></li>
							</ul>
						</li>
					</ul>
				</li>
			</ul>
		</nav><!--/.navbar-collapse -->
	</div><!-- /.container -->
</header><!-- /.navbar -->