<?php
/**
 * Element block - SideBar Dashboard
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements.sidebar
 * @since       yamatoya.sys v 1.0
 */
?><ul class="sidebar-nav">
	<li class="sidebar-brand"><span><i class="fa fa-tachometer"></i>&nbsp;ダッシュボード</span></li>
	<li class="active"><a href="<?php $this->Html->url("/admin/cpanel/dashboard", true); ?>">現在の予約状況</a></li>
</ul>