<?php
/**
 * Element block - SideBar Order
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.View.Themed.Cpanel.Elements.sidebar
 * @since       yamatoya.sys v 1.0
 */
?><ul class="sidebar-nav">
	<li class="sidebar-brand"><span><i class="fa fa-calendar"></i>&nbsp;予約管理</span></li>
	<li<?php echo ($this->request->action == 'admin_order_list')? ' class="active"': null; ?>><a href="<?php echo $this->Html->url("/admin/cpanel/order_list", true); ?>">予約一覧</a></li>
	<li<?php echo ($this->request->action == 'admin_order_entry')? ' class="active"': null; ?>><a href="<?php echo $this->Html->url("/admin/cpanel/order_entry", true); ?>">新規予約作成</a></li>
	<?php if ($this->request->action == 'admin_order_detail'): ?><li class="active"><a href="#">予約情報詳細</a></li><?php endif; echo PHP_EOL; ?>
	<?php if ($this->request->action == 'admin_order_edit'): ?><li class="active"><a href="#">予約情報編集</a></li><?php endif; echo PHP_EOL; ?>
	<li class="sidebar-brand"><span><i class="fa fa-print"></i>&nbsp;印刷物</span></li>
	<li><a href="<?php echo $this->Html->url("/course-menu/ja.html", true); ?>" target="_blank">コース：日本料理<i class="fa fa-share-square"></i></a></li>
	<li><a href="<?php echo $this->Html->url("/course-menu/fr.html", true); ?>" target="_blank">コース：西洋料理<i class="fa fa-share-square"></i></a></li>
	<li><a href="<?php echo $this->Html->url("/course-menu/ch.html", true); ?>" target="_blank">コース：中国料理<i class="fa fa-share-square"></i></a></li>
	<li><a href="<?php echo $this->Html->url("/select-menu/index.html", true); ?>" target="_blank">セレクトメニュー<i class="fa fa-share-square"></i></a></li>
	<li><a href="<?php echo $this->Html->url("/all/index.html", true); ?>" target="_blank">すべて印刷<i class="fa fa-share-square"></i></a></li>
	<li><a href="<?php echo $this->Html->url("/okosama/index.html", true); ?>" target="_blank">お子様メニュー<i class="fa fa-share-square"></i></a></li>
</ul>