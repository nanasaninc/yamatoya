/* ===========================================================
 * Yamatoya: common.js
 * ===========================================================
 * Copyright 2013 nanasan Inc.
 * ========================================================== */

// COMMON FUNCTIONS
// ================

var CommonFn = {

  /**
   * 全角文字を半角文字に変更
   *
   * @param   element e
   * @return  void
   */
  alphaNumeric: function (e) {
    value = $(e).val();
    value = value.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
      return String.fromCharCode(s.charCodeAt(0)-0xFEE0);
    });
    $(e).val(value);
  },

  /**
   * メニュー情報入力フォームの切り替え
   *
   * @param   element e
   * @return  void
   */
  menuSwitch: function () {
    $('#course, #select').removeClass('hide');
    $.each($('select, input:radio, input:checkbox'), function() {
      $(this).removeAttr('disabled');
    });
    switch ($('#OrderMenuTypeId').val()) {
      case '1':
        disable = '#select';
        break;
      case '5':
        disable = '#course';
        break;
      default:
        disable = '#course, #select';
        break;
    }
    $(disable).addClass('hide');
    $.each($(disable).find('select, input:radio, input:checkbox'), function() {
      $(this).attr('disabled', 'disabled');
    });
  },

  /**
   * コースメニュー内容表示の切り替え
   *
   * @param   element e
   * @return  void
   */
  courseSwitch: function () {
    if ($('#OrderMenuTypeId').val() == '1') {
      $('.products').addClass('hide');
      $('#course' + $('.course-select').val()).removeClass('hide');
    }
  },

};

// STOP FORM ENTER KEY SUBMIT
// ==========================

$(function() {
  $("form").keypress(function(ev) {
    if ((ev.which && ev.which === 13) || (ev.keyCode && ev.keyCode === 13))
      return false;
    else
      return true;
  });
});
