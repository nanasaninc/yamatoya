+function ($) { "use strict";

  $(document).on('click', '.sidebar-toggle', function(e) {
    // Sidebar toggle.
    e.preventDefault();
    $(".wrapper").toggleClass("active");
    // Icon toggle.
    if ($('i.fa', this).hasClass('fa-arrow-right')) {
      $('i.fa', this).removeClass('fa-arrow-right').addClass('fa-arrow-left');
    } else {
      $('i.fa', this).removeClass('fa-arrow-left').addClass('fa-arrow-right');
    }
  });

}(window.jQuery);
