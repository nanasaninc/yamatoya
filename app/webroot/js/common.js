$(function() {
  // MobileSafari app mode anchors.
  var anchors = $('a.applink');
  $.each(anchors, function() {
    var url = $(this).attr('href');
    $(this).removeAttr('href').css('cursor', 'pointer');
    $(this).click(function() {
      location.href = url;
    });
  });
  // Popup window.
  $(".popup").fancybox({
    'overlayOpacity': 0.6,
    'overlayColor': '#000'
  });
  // ScrollPane
  $("#confirm #menu-list").jScrollPane();
  // Gallery
  if ($('.flipsnap').is('*')) {
    $.each($('.flipsnap'), function(i) {
      var fsnap = Flipsnap(this, { distance: 680 }),
          num   = $('.item', this).length,
          clfnc = function(sel) {
            $(sel).click(function() {
              index = $(sel).index(this);
              fsnap.moveToPoint(index);
            });
          };
      $(this).width(680 * num);
      // Popup click.
      clfnc('.namelists.num' + i + ' .popup');
      clfnc('.thumbnail.num' + i + ' .popup');
      clfnc('.selectBox.num' + i + ' .popup');
      // ...
    });
  }
  // Loader position.
  // var window_height = $(window).height(),
  //     loader_height = $('.mask .loader').height();
  // $('.mask .loader').css({ 'margin-top': ((window_height - loader_height) / 2), 'opacity': 1 });
  // FadeIn
  // $(window).load(function() {
    setTimeout(function() {
      $('.mask').stop().animate({ 'opacity': '0' }, 1000, function() {
        $(this).addClass('hide');
      });
    }, 700);
  // });
});
