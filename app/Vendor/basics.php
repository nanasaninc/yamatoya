<?php
/**
 * Custom Basic functionality.
 *
 * Core functions for including other source files, loading models and so forth.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Vendor
 * @since       yamatoya.sys v 1.0
 */

/**
 * n2b_h method
 * : 'nl' to 'br' and htmlspecialchars
 * : 再帰的にサニタイジング後、改行タグを挿入
 *
 * @param string|array $text
 * @param boolean $double
 * @param string $charset
 * @return string|array
 */
if (!function_exists('n2b_h')) {
	function n2b_h($text, $double = true, $charset = null) {
		if (is_array($text)) {
			$texts = array();
			foreach ($text as $key => $value) {
				$texts[$key] = n2b_h($value, $double, $charset);
			}
			return $texts;
		} else {
			return nl2br(h($text, $double, $charset));
		}
	}
}

/**
 * h_rn method
 * : htmlspecialchars and remove 'nl'
 * : 再帰的に改行コード削除後サニタイジング
 *
 * @param string|array $text
 * @param boolean $double
 * @param string $charset
 * @return string|array
 */
if (!function_exists('h_rn')) {
	function h_rn($text, $double = true, $charset = null) {
		if (is_array($text)) {
			$texts = array();
			foreach ($text as $key => $value) {
				$texts[$key] = h_rn($value, $double, $charset);
			}
			return $texts;
		} else {
			return h(str_replace(array("\r\n", "\r", "\n"), '', $text), $double, $charset);
		}
	}
}

/**
 * pds method
 * : post data sanitizing
 * : POSTデータのサニタイジング
 *
 * @param string|array $text
 * @param boolean $double
 * @param string $charset
 * @return string|array
 */
if (!function_exists('pds')) {
	function pds($text = null, $double = true, $charset = null) {
		$trim_data = array(
			'Ⅰ' => 'I', 'Ⅱ' => 'II', 'Ⅲ' => 'III', 'Ⅳ' => 'IV', 'Ⅴ' => 'V', 'Ⅵ' => 'VI', 'Ⅶ' => 'VII', 'Ⅷ' => 'VIII', 'Ⅸ' => 'IX', 'Ⅹ' => 'X',
			'ⅰ' => 'i', 'ⅱ' => 'ii', 'ⅲ' => 'iii', 'ⅳ' => 'iv', 'ⅴ' => 'v', 'ⅵ' => 'vi', 'ⅶ' => 'vii', 'ⅷ' => 'viii', 'ⅸ' => 'ix', 'ⅹ' => 'x',
			'①' => '(1)', '②' => '(2)', '③' => '(3)', '④' => '(4)', '⑤' => '(5)', '⑥' => '(6)', '⑦' => '(7)', '⑧' => '(8)', '⑨' => '(9)', '⑩' => '(10)', '⑪' => '(11)', '⑫' => '(12)', '⑬' => '(13)', '⑭' => '(14)', '⑮' => '(15)', '⑯' => '(16)', '⑰' => '(17)', '⑱' => '(18)', '⑲' => '(19)', '⑳' => '(20)',
			'㊤' => '(上)', '㊥' => '(中)', '㊦' => '(下)', '㊧' => '(左)', '㊨' => '(右)',
			'㍉' => 'ミリ', '㍍' => 'メートル', '㌔' => 'キロ', '㌘' => 'グラム', '㌧' => 'トン', '㌦' => 'ドル', '㍑' => 'リットル', '㌫' => 'パーセント', '㌢' => 'センチ',
			'㎜' => 'mm', '㎝' => 'cm', '㎡' => 'm2', '㎏' => 'kg', '㏍' => 'K.K.', '℡' => 'TEL', '№' => 'No.',
			'㍻' => '平成', '㍼' => '昭和', '㍽' => '大正', '㍾' => '明治',
			'㈱' => '(株)', '㈲' => '(有)', '㈹' => '(代)',
		);
		if (is_null($text))
			return '';
		if (is_array($text)) {
			$texts = array();
			foreach ($text as $key => $value) {
				$texts[$key] = pds($value, $double, $charset);
			}
			return $texts;
		} else {
			return h(trim(mb_convert_kana(str_replace(array_keys($trim_data), array_values($trim_data), $text), 'asK', 'UTF-8')), $double, $charset);
		}
	}
}

/**
 * pdr method
 * : post data sanitizing and return 'nl'
 * : POSTデータのサニタイジング後、改行のエスケープを戻す
 *
 * @param string|array $text
 * @param boolean $double
 * @param string $charset
 * @return string|array
 */
if (!function_exists('pdr')) {
	function pdr($text, $double = true, $charset = null) {
		if (is_array($text)) {
			$texts = array();
			foreach ($text as $key => $value) {
				$texts[$key] = pdr($value, $double, $charset);
			}
			return $texts;
		} else {
			return stripcslashes(preg_replace('/\\\\n/', "\n", pds($text, $double, $charset)));
		}
	}
}

/**
 * rds method
 * : random string
 * : ランダム文字列作成
 *
 * @param integer $length
 * @return string
 */
if (!function_exists('rds')) {
	function rds($length = 8) {
		$chars  = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_';
		$result = '';
		for ($i = 0; $i < $length; $i++) {
			$rand   = rand(0, (mb_strlen($chars) - 1));
			$result.= $chars[$rand];
		}
		return $result;
	}
}

/**
 *  sp_adr method
 * : split address
 * : 住所分割
 *
 * @param string $address
 * @return string
 */
if (!function_exists('sp_adr')) {
	function sp_adr($address = '') {
		$match = array();
		preg_match('/^(北海道|青森県|岩手県|秋田県|山形県|宮城県|福島県|新潟県|栃木県|群馬県|茨城県|埼玉県|千葉県|東京都|神奈川県|山梨県|静岡県|長野県|富山県|石川県|福井県|岐阜県|愛知県|三重県|奈良県|和歌山県|滋賀県|京都府|大阪府|兵庫県|岡山県|広島県|鳥取県|島根県|山口県|香川県|徳島県|愛媛県|高知県|福岡県|佐賀県|長崎県|熊本県|大分県|宮崎県|鹿児島県|沖縄県)(北松浦郡鹿町町|.+?郡.+?町|.+?郡.+?村|宇陀市|奥州市|上越市|黒部市|豊川市|姫路市|.+?[^0-9一二三四五六七八九十]区|四日市市|廿日市市|.+?市|.+?町|.+?村)(.*)$/u', $address, $match);
		$return['prefecture'] = !empty($match[1]) ? $match[1] : '';
		$return['city']       = !empty($match[2]) ? $match[2] : '';
		$return['street']     = !empty($match[3]) ? $match[3] : '';
		return $return;
	}
}

/**
 *  sp_dotw_jp method
 * : split days of the week.
 * : 英語の曜日を日本の曜日に差し替え
 *
 * @param string $date
 * @return string
 */
if (!function_exists('sp_dotw_jp')) {
	function sp_dotw_jp($date = '') {
		$searches = array('Monday', 'Mon', 'Tuesday', 'Tue', 'Wednesday', 'Wed', 'Thursday', 'Thu', 'Friday', 'Fri', 'Saturday', 'Sat', 'Sunday', 'Sun');
		$replaces = array('月曜日', '月', '火曜日', '火', '水曜日', '水', '木曜日', '木', '金曜日', '金', '土曜日', '土', '日曜日', '日');
		$return = str_replace($searches, $replaces, $date);
		return $return;
	}
}

/**
 * btn_check method
 * : Submit button name check.
 * : 送信ボタンのname属性のチェック(type=imageの場合、座標が付与されるため)
 *
 * @param   string $name
 * @param   array  $request
 * @return  boolean
 */
if (!function_exists('btn_check')) {
	function btn_check($name, $request) {
		if (empty($request))
			return false;
		foreach ($request as $key => $value) {
			preg_match('/^(' . $name . ')(_[xy])?$/', $key, $matches);
			if ((boolean)$matches)
				return true;
		}
		return false;
	}
}

/**
 * rp_d method
 * : Routing prefix delete for action name.
 * : アクション名からルーティング接頭辞を除去
 *
 * @param   array  $params
 * @return  string
 */
if (!function_exists('rp_d')) {
	function rp_d($params) {
		return isset($params['prefix'])?
			str_replace("{$params['prefix']}_", '', $params['action']):
			$params['action'];
	}
}
