<?php
/**
 * MenuType model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * MenuType model.
 *
 * @property    Menu         $Menu
 * @property    ProductType  $ProductType
 */
class MenuType extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'MenuType';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'menu_types';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array(
		'Tree',
	);

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array(
		'Menu' => array(
			'className'  => 'Menu',
			'foreignKey' => 'menu_type_id',
			'dependent'  => false,
		),
		'ProductType' => array(
			'className'  => 'ProductType',
			'foreignKey' => 'menu_type_id',
			'dependent'  => false,
		),
	);

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array();

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array();

/**
 * Model methods.
 */
	/**
	 * getSelect method
	 *
	 * @param    mixed   $parent_id
	 * @return   array
	 */
	public function getSelect($parent_id = null) {
		// Find
		return $this->find('list', array(
			'conditions' => array(
				'parent_id' => $parent_id,
			),
			'order' => array(
				'lft' => 'asc'
			),
		));
	}

	/**
	 * getName method
	 *
	 * @param   mixed   $id
	 * @return  array
	 */
	public function getName($id = null) {
		// Find
		return Hash::extract($this->find('first', array(
			'conditions' => array(
				'MenuType.id'  => $id,
			),
			'fields' => array('MenuType.name'),
		)), 'MenuType.name');
	}

	/**
	 * getIdOfSlug method
	 *
	 * @param   mixed   $slug
	 * @return  array
	 */
	public function getIdOfSlug($slug = null) {
		// Find
		return Hash::extract($this->find('list', array(
			'conditions' => array('MenuType.name_en' => $slug),
			'fields'     => array('MenuType.id'),
		)), '{n}');
	}

	/**
	 * getChildIdOfSlug method
	 *
	 * @param   mixed   $slug
	 * @return  array
	 */
	public function getChildIdOfSlug($slug = null) {
		// Find
		return Hash::extract($this->find('list', array(
			'conditions' => array('MenuType.parent_id' => $this->getIdOfSlug($slug)),
			'fields'     => array('MenuType.id'),
		)), '{n}');
	}

	/**
	 * getChildNameListOfSlug method
	 *
	 * @param   mixed   $slug
	 * @return  array
	 */
	public function getChildNameListOfSlug($slug = null) {
		// Find
		return $this->find('list', array(
			'conditions' => array('MenuType.parent_id' => $this->getIdOfSlug($slug)),
			'fields'     => array('MenuType.name', 'MenuType.id'),
		));
	}

	/**
	 * getChildMultipleListOfSlug method
	 *
	 * @param   mixed   $slug
	 * @return  array
	 */
	public function getChildMultipleListOfSlug($slug = null) {
		// Find
		return $this->find('list', array(
			'conditions' => array('MenuType.parent_id' => $this->getIdOfSlug($slug)),
			'fields'     => array('MenuType.name', 'MenuType.multiple'),
		));
	}

	/**
	 * getParentId method
	 *
	 * @return  array
	 */
	public function getParentId() {
		// Find
		return Hash::extract($this->find('list', array(
			'conditions' => array('MenuType.parent_id' => null),
			'fields'     => array('MenuType.id'),
		)), '{n}');
	}

}
