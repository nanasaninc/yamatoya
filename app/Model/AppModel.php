<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	/**
	 * Behavior
	 *
	 * @var array
	 */
	public $actsAs = array(
		'Cakeplus.AddValidationRule',
		'Cakeplus.ValidationErrorI18n',
		'Cakeplus.ValidationPatterns',
	);

	/**
	 * exists method
	 *
	 * Extend for SoftDeleteBehavior.
	 * SoftDeleteBehavior用処理
	 *
	 * @param   mixed   $id
	 * @return  boolean
	 * @link    https://github.com/CakeDC/utils#softdelete-behavior
	 */
	public function exists($id = null) {
		if ($this->Behaviors->attached('SoftDelete')) {
			return $this->existsAndNotDeleted($id);
		} else {
			return parent::exists($id);
		}
	}

	/**
	 * cancelDeleted method
	 *
	 * Cancel the delete for SoftDeleteBehavior.
	 * 削除を取り消す。(SoftDeleteBehavior)
	 *
	 * @param   mixed   $id
	 * @return  array
	 */
	public function cancelDeleted($id = null) {
		if ($this->Behaviors->attached('SoftDelete')) {
			$this->id = $id;
			return (boolean)$this->save(array(
				'deleted'  => null,
				'modified' => false,
			), false);
		} else {
			return false;
		}
	}

	/**
	 * multiWordConditions method
	 *
	 * Multi keyword search on multi fields for Search Plugin.
	 * SearchPlugin用複数キーワード検索(複数フィールド対応)
	 *
	 * @param   array   $data
	 * @return  array
	 */
	public function multiWordConditions($data = array()) {
		if (isset($data['field'])) {
			$field  = mb_convert_kana($data['field'], "s", "UTF-8");
			$fields = explode(',', $field);
		} else {
			$fields = array('name');
		}
		$keyword  = mb_convert_kana($data['keyword'], "s", "UTF-8");
		$keywords = preg_split('/[\s|\x{3000}]+/u', $keyword);

		if(count($keywords) < 2) {
			$conditions['OR'] = array();
			foreach ($fields as $field) {
				$conditions['OR']["{$this->alias}.{$field} LIKE"] = "%{$keyword}%";
			}
		} else {
			$conditions['AND'] = array();
			foreach($keywords as $count => $keyword) {
				$condition['OR'] = array();
				foreach ($fields as $field) {
					$condition['OR']["{$this->alias}.{$field} LIKE"] = "%{$keyword}%";
				}
				array_push($conditions['AND'], $condition);
			}
		}
		return $conditions;
	}

	/**
	 * unbindValidation method
	 *
	 * Unbinds validation rules and optionally sets the remaining rules to required.
	 * 必要に応じてバリデーション規則を解除、および必須設定を更新します。
	 *
	 * 1. $type
	 *   'remove' = 指定したフィールドに対するバリデーションを削除する。
	 *   'keep'   = 指定したフィールド以外のバリデーションを全て削除する。
	 * 2. $require
	 *   true     = 残ったフィールドにrequire属性を付与する。
	 *   false    = 残ったフィールドのrequire属性はそのまま。
	 *
	 * @param   string  $type
	 * @param   array   $fields
	 * @param   boolean $require
	 * @return  void
	 */
	public function unbindValidation($type, $fields, $require = false) {
		if ($type === 'remove') {
			$this->validate = array_diff_key($this->validate, array_flip($fields));
		} elseif ($type === 'keep') {
			$this->validate = array_intersect_key($this->validate, array_flip($fields));
		}
		if ($require === true) {
			foreach ($this->validate as $field => $rules) {
				if (is_array($rules)) {
					$rule = key($rules);
					$this->validate[$field][$rule]['required'] = true;
				} else {
					$ruleName = (ctype_alpha($rules))
						? $rules
						: 'required';
					$this->validate[$field] = array(
						$ruleName => array(
							'rule'     => $rules,
							'required' => true,
						)
					);
				}
			}
		}
	}

/**
 * Original validation rule methods.
 */
	/**
	 * notEmpty2Fields method
	 *
	 * NotEmpty check the chain up two fields.
	 * NotEmptyの2フィールド連鎖チェック
	 *
	 * @param   string  $check
	 * @param   string  $field
	 * @return  boolean
	 */
	public function notEmpty2Fields($check, $field) {
		// Get check value.
		$value = array_shift($check);
		// Double notEmpty()
		return Validation::notEmpty($value) && Validation::notEmpty($this->data[$this->alias][$field]);
	}

	/**
	 * hiraganaOnly2Fields method
	 *
	 * hiraganaOnly check the chain up two fields.
	 * hiraganaOnlyの2フィールド連鎖チェック
	 *
	 * @param   string  $check
	 * @param   string  $field
	 * @return  boolean
	 */
	public function hiraganaOnly2Fields($check, $field) {
		// Double hiraganaOnly()
		return AddValidationRuleBehavior::hiraganaOnly($this, $check) && AddValidationRuleBehavior::hiraganaOnly($this, array($field => $this->data[$this->alias][$field]));
	}

	/**
	 * katakanaOnly2Fields method
	 *
	 * katakanaOnly check the chain up two fields.
	 * katakanaOnlyの2フィールド連鎖チェック
	 *
	 * @param   string  $check
	 * @param   string  $field
	 * @return  boolean
	 */
	public function katakanaOnly2Fields($check, $field) {
		// Double katakanaOnly()
		return AddValidationRuleBehavior::katakanaOnly($this, $check) && AddValidationRuleBehavior::katakanaOnly($this, array($field => $this->data[$this->alias][$field]));
	}

}
