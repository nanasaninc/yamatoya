<?php
/**
 * ProductType model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * ProductType model.
 *
 * @property    Product      $Product
 * @property    MenuType     $MenuType
 */
class ProductType extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'ProductType';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'product_types';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array();

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array(
		'Product' => array(
			'className'  => 'Product',
			'foreignKey' => 'product_type_id',
			'dependent'  => false,
		),
	);

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'MenuType' => array(
			'className'  => 'MenuType',
			'foreignKey' => 'menu_type_id',
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array();

}
