<?php
/**
 * MenuProduct model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * MenuProduct model.
 *
 * @property    Menu         $Menu
 * @property    Product      $Product
 */
class MenuProduct extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'MenuProduct';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'menu_products';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array();

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array();

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'Menu' => array(
			'className'  => 'Menu',
			'foreignKey' => 'menu_id',
		),
		'Product' => array(
			'className'  => 'Product',
			'foreignKey' => 'product_id',
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array();

}
