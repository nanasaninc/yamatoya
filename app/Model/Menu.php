<?php
/**
 * Menu model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * Menu model.
 *
 * @property    OrderMenu    $OrderMenu
 * @property    MenuType     $MenuType
 * @property    Product      $Product
 */
class Menu extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'Menu';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'menus';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array(
		'Containable',
	);

	/**
	 * Virtual fields
	 *
	 * @var     array
	 */
	public $virtualFields = array(
		'pulldown_name'    => 'CONCAT(`Menu`.`name`, IF(`Menu`.`kana` IS NULL, "", CONCAT(" (", `Menu`.`kana`, ")")))',
		'difference_price' => '`Menu`.`price` - `Menu`.`standard_price`',
	);

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array(
		'OrderMenu' => array(
			'className'  => 'OrderMenu',
			'foreignKey' => 'menu_id',
			'dependent'  => false,
		),
	);

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array(
		'MenuProduct' => array(
			'className'  => 'MenuProduct',
			'foreignKey' => 'menu_id',
			'dependent'  => false,
		),
	);

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'MenuType' => array(
			'className'  => 'MenuType',
			'foreignKey' => 'menu_type_id',
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

	/**
	 * contain settings.
	 *
	 * @var     array
	 */
	public $contain = array(
		'MenuType' => array(
			'id',
			'name',
			'name_en',
			'multiple',
		),
		'MenuProduct' => array(
			'Product' => array(
				'ProductType' => array(
					'name',
					'slug',
				),
			),
		),
	);

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array();

/**
 * Model methods.
 */
	/**
	 * getMenu method
	 *
	 * @param   integer $menu_type_id
	 * @return  array
	 */
	public function getMenu($menu_type_id = null) {
		return $this->find('all', array(
			'contain' => $this->contain,
			'conditions' => array(
				'Menu.menu_type_id' => $menu_type_id,
			),
		));
	}

	/**
	 * getMenuIndexId method
	 *
	 * @param   integer $menu_type_id
	 * @return  array
	 */
	public function getMenuIndexId($menu_type_id = null) {
		$menus = $this->find('all', array(
			'contain' => $this->contain,
			'conditions' => array(
				'Menu.menu_type_id' => $menu_type_id,
			),
		));
		foreach ($menus as $menu) {
			$data[$menu['Menu']['id']] = $menu;
		}
		return $data;
	}

	/**
	 * getDetail method
	 *
	 * @param   integer $menu_id
	 * @return  array
	 */
	public function getDetail($menu_id = null) {
		return $this->find('first', array(
			'contain' => $this->contain,
			'conditions' => array(
				'Menu.id' => $menu_id,
			),
		));
	}

	/**
	 * getMenuList method
	 *
	 * @param   integer $menu_id
	 * @return  array
	 */
	public function getMenuList($menu_type_id = null) {
		return $this->find('list', array(
			'conditions' => array(
				'Menu.menu_type_id' => $menu_type_id,
			),
			'fields' => array('Menu.pulldown_name'),
		));
	}

	/**
	 * getRequiredList method
	 *
	 * @param   integer $menu_id
	 * @return  array
	 */
	public function getRequiredList($menu_type_id = null) {
		return $this->find('list', array(
			'conditions' => array(
				'Menu.menu_type_id' => $menu_type_id,
			),
			'fields' => array('Menu.required'),
		));
	}

	/**
	 * getMenuListByType method
	 *
	 * @param   integer $menu_id
	 * @return  array
	 */
	public function getMenuListByType($menu_type_id = null) {
		// Set menu type parent id.
		if ($menu_type_id == null)
			$menu_type_id = $this->MenuType->getParentId();
		else
			$menu_type_id = array($menu_type_id);
		// Get menu type ids
		foreach ($menu_type_id as $parent_id)
			$menu_types[$parent_id] = Hash::extract($this->MenuType->children($parent_id, false, array('MenuType.id')), '{n}.MenuType.id');
		// Get menu list.
		foreach ($menu_types as $parent_id => $ids) {
			// Get menu type list.
			$type    = $this->MenuType->find('list');
			$type_en = $this->MenuType->find('list', array('fields' => array('MenuType.name_en')));
			// Set menu type.
			foreach ($ids as $id) {
				$menus[$type_en[$parent_id]][$type[$id]] = $this->getMenuList($id);
			}
		}
		return $menus;
	}

}
