<?php
/**
 * User model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User model.
 *
 */
class User extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'User';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'users';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array(
		'Search.Searchable',
		'Utils.SoftDelete',
	);

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array();

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array();

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array(
		'login_id' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'ログインIDが入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
		),
		'password' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'パスワードが入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
		),
	);

/**
 * Model hook methods.
 */
	/**
	 * beforeSave method
	 *
	 * @param   array   $options
	 * @return  boolean
	 */
	public function beforeSave($options = array()) {
		$password &= $this->data[$this->alias]['password'];
		$password  = AuthComponent::password($password);
	}

}
