<?php
/**
 * Product model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * Product model.
 *
 * @property    Menu         $Menu
 * @property    ProductType  $ProductType
 */
class Product extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'Product';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'products';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array();

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array(
		'MenuProduct' => array(
			'className'  => 'MenuProduct',
			'foreignKey' => 'product_id',
			'dependent'  => false,
		),
	);

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'ProductType' => array(
			'className'  => 'ProductType',
			'foreignKey' => 'product_type_id',
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array();

}
