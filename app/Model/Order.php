<?php
/**
 * Order model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * Order model.
 *
 * @property    OrderMenu    $OrderMenu
 * @property    Prefecture   $Prefecture
 */
class Order extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'Order';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'orders';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array(
		'Containable',
		'Search.Searchable',
		'Utils.SoftDelete',
	);

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array();

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array(
		'OrderMenu' => array(
			'className'  => 'OrderMenu',
			'foreignKey' => 'order_id',
			'dependent'  => true,
		),
	);

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'MenuType' => array(
			'className'  => 'MenuType',
			'foreignKey' => 'menu_type_id',
			'dependent'  => false,
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array(
		'Menu' => array(
			'className'             => 'Menu',
			'joinTable'             => 'order_menus',
			'with'                  => 'OrderMenu',
			'foreignKey'            => 'order_id',
			'associationForeignKey' => 'menu_id',
			'unique'                => true,
		),
	);

	/**
	 * contain settings
	 *
	 * @var     array
	 */
	public $contain = array(
		'MenuType' => array(
			'id',
			'name',
			'name_en',
		),
		'Menu' => array(
			'MenuType' => array(
				'id',
				'name',
				'name_en',
				'multiple',
			),
			'MenuProduct' => array(
				'Product' => array(
					'ProductType' => array(
						'name',
						'slug',
					),
				),
			),
		),
		'OrderMenu.menu_id',
	);

/**
 * Setting variables.
 */
	/**
	 * modeAdmin setting.
	 *
	 * @var     boolean
	 */
	protected $_modeAdmin = false;

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array(
		'order_name' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => '予約IDが入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
			'alphaNumber' => array(
				'rule'       => array('alphaNumber'),
				'message'    => '半角英数以外が入力されています。',
			),
			'isUnique' => array(
				'rule'       => array('isUnique'),
				'message'    => 'その予約IDは既に使われています。',
			),
		),
		'groom_name' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'ご新郎様氏名が入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
		),
		'bride_name' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'ご新婦様氏名が入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
		),
		'date' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'ご予約日が入力されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
			'date' => array(
				'rule'       => array('date'),
				'message'    => '正しい日付を入力してください。',
			),
		),
	);

/**
 * Search settings.
 */
	/**
	 * Filter Arguments
	 *
	 * @var array
	 */
	public $filterArgs = array(
		array(
			'name'   => 'order_name',
			'type'   => 'like',
		),
		array(
			'name'   => 'keyword',
			'type'   => 'query',
			'method' => 'multiWordConditions',
		),
		array(
			'name'   => 'menu_type_id',
			'type'   => 'value',
		),
		array(
			'name'   => 'date_from',
			'type'   => 'value',
			'field'  => 'Order.datetime >='
		),
		array(
			'name'   => 'date_to',
			'type'   => 'value',
			'field'  => 'Order.datetime <='
		),
	);

/**
 * Hook methods.
 */
	/**
	 * afterFind method
	 *
	 * @param   array   $results
	 * @param   boolean $primary
	 * @return  array
	 */
	public function afterFind($results = array(), $primary = false) {
		switch ($this->findQueryType) {
			case 'all':
			case 'first':
				$count = count($results);
				for ($i = 0; $i < $count; $i++) {
					// Decompose the datetime.
					list($date, $time)            = explode(' ', $results[$i]['Order']['datetime']);
					list($hour, $minute, $second) = explode(':', $time);
					// Set datetime for results.
					$results[$i]['Order']['date']   = $date;
					$results[$i]['Order']['hour']   = $hour;
					$results[$i]['Order']['minute'] = $minute;
					// Set menu type for results.
					if (empty($results[$i]['Order']['menu_type_id']))
						$results[$i]['MenuType']['name'] = '未選択';
					// Set menu IDs.
					$results[$i]['Order']['menus'] = Hash::extract($this->OrderMenu->find('list', array(
						'conditions' => array('OrderMenu.order_id' => $results[$i]['Order']['id']),
						'fields'     => array('OrderMenu.menu_id'),
					)), '{n}');
				}
				break;
		}
		return $results;
	}

	/**
	 * afterValidate method
	 *
	 * @return  void
	 */
	public function afterValidate() {
	}

	/**
	 * beforeSave method
	 *
	 * @param   array   $options
	 * @return  boolean
	 */
	public function beforeSave($options = array()) {
		// Construct a datetime.
		if ($this->_modeAdmin)
			$this->data['Order']['datetime'] = "{$this->data['Order']['date']} {$this->data['Order']['hour']}:{$this->data['Order']['minute']}:00";
		// Set menu type id.
		if (isset($this->data['Order']['menu_type_id'])) {
			if (empty($this->data['Order']['menu_type_id']))
				$this->data['Order']['menu_type_id'] = null;
		}
		return true;
	}

	/**
	 * afterSave method
	 *
	 * @param   boolean $created
	 * @return  boolean
	 */
	public function afterSave($created) {
		// Get data the saved.
		$this->id = $this->getID();
		$this->contain($this->contain);
		$data = $this->read();
		unset($data['Order']['history']);
		// Save data history.
		$this->save(array('history' => serialize($data), 'modified' => false), array('callbacks' => false));
	}

/**
 * Model methods.
 */
	/**
	 * modeAdmin method
	 *
	 * @return  boolean
	 */
	public function modeAdmin($flag = false) {
		$this->_modeAdmin = $flag;
	}

	/**
	 * stateCount method
	 *
	 * @return  mixed
	 */
	public function stateCount() {
		$states = $this->State->find('list');
		foreach ($states as $id => $name) {
			$results[$id] = array(
				'name'  => $name,
				'count' => $this->find('count', array(
					'conditions' => array(
						'Order.state_id' => $id,
					),
				)),
			);
		}
		return $results;
	}

}
