<?php
/**
 * OrderMenu model.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Model
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppModel', 'Model');

/**
 * OrderMenu model.
 *
 * @property    Order        $Order
 * @property    Menu         $Menu
 */
class OrderMenu extends AppModel {

	/**
	 * Model name
	 *
	 * @var     string
	 */
	public $name = 'OrderMenu';

	/**
	 * Use table
	 *
	 * @var     mixed
	 */
	public $useTable = 'order_menus';

	/**
	 * Behavior
	 *
	 * @var     array
	 */
	public $actsAs = array();

/**
 * Accosiations.
 */
	/**
	 * hasOne associations
	 *
	 * @var     array
	 */
	public $hasOne = array();

	/**
	 * hasMany associations
	 *
	 * @var     array
	 */
	public $hasMany = array();

	/**
	 * belongsTo associations
	 *
	 * @var     array
	 */
	public $belongsTo = array(
		'Order' => array(
			'className'  => 'Order',
			'foreignKey' => 'order_id',
		),
		'Menu' => array(
			'className'  => 'Menu',
			'foreignKey' => 'menu_id',
		),
	);

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var     array
	 */
	public $hasAndBelongsToMany = array();

/**
 * Validates.
 */
	/**
	 * Validation rules
	 *
	 * @var     array
	 */
	public $validate = array(
		'menu_id' => array(
			'notEmpty' => array(
				'rule'       => array('notEmpty'),
				'message'    => 'メニューが選択されておりません。',
				'required'   => true,
				'allowEmpty' => false,
			),
		),
	);

}
