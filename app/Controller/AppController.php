<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Controller
 * @since       yamatoya.sys v 1.0
 */
App::uses('Controller', 'Controller');
App::uses('CakeNumber', 'Utility');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 */
class AppController extends Controller {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array(
		// 'DebugKit.Toolbar',
		'RequestHandler',
		'Session',
		'Search.Prg',
	);

	/**
	 * Helpers
	 *
	 * @var array
	 */
	public $helpers = array(
		'Form',
		'Html',
		'Number',
		'Paginator',
		'Session',
		'Text',
		'Time',
	);

	/**
	 * beforeFilter method
	 *
	 * @return void
	 */
	public function beforeFilter() {}

	/**
	 * beforeRender method
	 *
	 * @return void
	 */
	public function beforeRender() {
		// CakeNumberクラスに貨幣種別"JPY"を追加
		CakeNumber::addFormat('JPY', array(
			'wholeSymbol'      => '円',
			'wholePosition'    => 'after',
			'fractionSymbol'   => '',
			'fractionPosition' => 'after',
			'zero'             => 0,
			'places'           => 0,
			'thousands'        => ',',
			'decimals'         => '.',
			'negative'         => '()',
			'escape'           => true,
		));
		// CakeNumberクラスの標準貨幣種別を"JPY"に設定
		CakeNumber::defaultCurrency('JPY');
	}

	/**
	 * _clearCache method
	 *
	 * @return  void
	 */
	public function _clearCache() {
		// Cache clear.
		$config_list = Cache::configured();
		foreach ($config_list as $value) {
			Cache::clear(false, $value);
		}
		clearCache();
	}

}
