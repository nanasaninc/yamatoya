<?php
/**
 * Yamatoya controller.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Controller
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppController', 'Controller');

/**
 * Yamatoya controller
 */
class YamatoyaController extends AppController {

	/**
	 * Controller name
	 *
	 * @var     string
	 */
	public $name = 'Yamatoya';

	/**
	 * Layout
	 *
	 * @var string
	 */
	public $layout = 'yamatoya-default';

	/**
	 * Use model
	 *
	 * @var     mixed
	 */
	public $uses = array('Order');

	/**
	 * Components
	 *
	 * @var     array
	 */
	public $components = array(
		// 'Security',
	);

	/**
	 * Helpers
	 *
	 * @var     array
	 */
	public $helpers = array();

/**
 * Controller hook methods.
 */
	/**
	 * beforeFilter method
	 *
	 * @return  void
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		// Security Component settings.
		// $this->Security->unlockedActions   = array('index');
		// $this->Security->validatePost      = false;
		// $this->Security->blackHoleCallback = '_blackHole';
		// Initialize.
		$session = array();
		// Get cart and order sessions.
		if ($this->Session->check('Session'))
			$session = $this->Session->read('Session');
		elseif ($this->action != 'auth' && $this->action != 'printing')
			$this->redirect('/auth');
		// Set controller valiables.
		$this->session = $session;
	}

	/**
	 * beforeRender method
	 *
	 * @return void
	 */
	public function beforeRender() {
		parent::beforeRender();
		// Set form valiables.
		if (!empty($this->session))
			$this->request->data['Session'] = $this->session;
	}

/**
 * Security Component methods.
 */
	/**
	 * _blackHole method
	 *
	 * @return  void
	 */
	public function _blackHole() {
		// Redirect.
		$this->redirect('/');
	}

/**
 * Action methods.
 */
	/**
	 * auth method
	 * : 認証画面
	 *
	 * @return  void
	 */
	public function auth() {
		// Session check.
		if ($this->Session->check('Session'))
			$this->redirect('/');
		// Authorization
		if ($this->request->is('post') || $this->request->is('put')) {
			// Get order data.
			$order = $this->Order->find('first', array(
				'contain'    => $this->Order->contain,
				'conditions' => array(
					'Order.order_name' => $this->request->data['Order']['order_name'],
				),
			));
			// Check
			if (!empty($order)) {
				// Success.
				$this->Session->write('Session', $order);
				if (!empty($order['Order']['menu_type_id'])) {
					$this->redirect('/confirm');
				} else {
					$this->redirect('/');
				}
			} else {
				// Failed.
				$this->Session->setFlash('予約IDに誤りがあります。', 'alert', array('class' => 'error'));
			}
		}
		// Set body ID.
		$this->set('body_id', 'auth');
	}

	/**
	 * unauth method
	 * : 認証解除処理
	 *
	 * @return  void
	 */
	public function unauth() {
		// Session delete.
		$this->Session->delete('Session');
		$this->redirect('/auth');
	}

	/**
	 * index method
	 * : トップページ(メニュー選択画面)
	 *
	 * @return  void
	 * @url     /
	 * @url     /course
	 * @url     /japanese
	 * @url     /french
	 * @url     /chinese
	 * @url     /select
	 */
	public function index() {
		// Request type check.
		if ($this->request->is('get')) {
			if (isset($this->request->pass[0])) {
				// Page check.
				if (array_search($this->request->pass[0], array('course', 'japanese', 'french', 'chinese', 'select')) === false)
					$this->redirect("/");
				// Page switch.
				switch ($this->request->pass[0]) {
					case 'course':
						$body_id      = 'course';
						$wrap_class   = 'course';
						$render_view  = 'course_top';
						break;
					case 'japanese':
						$type_id      = reset($this->Order->Menu->MenuType->getIdOfSlug('Course'));
						$type_ids     = $this->Order->Menu->MenuType->getIdOfSlug('Japanese');
						$body_id      = 'course';
						$wrap_class   = 'japanese';
						$render_view  = 'course';
						// Page.
						if (isset($this->request->query['p'])) {
							$this->set('p', $this->request->query['p']);
						}
						break;
					case 'french':
						$type_id      = reset($this->Order->Menu->MenuType->getIdOfSlug('Course'));
						$type_ids     = $this->Order->Menu->MenuType->getIdOfSlug('French');
						$body_id      = 'course';
						$wrap_class   = 'french';
						$render_view  = 'course';
						// Page.
						if (isset($this->request->query['p'])) {
							$this->set('p', $this->request->query['p']);
						}
						break;
					case 'chinese':
						$type_id      = reset($this->Order->Menu->MenuType->getIdOfSlug('Course'));
						$type_ids     = $this->Order->Menu->MenuType->getIdOfSlug('Chinese');
						$body_id      = 'course';
						$wrap_class   = 'chinese';
						$render_view  = 'course';
						// Page.
						if (isset($this->request->query['p'])) {
							$this->set('p', $this->request->query['p']);
						}
						break;
					case 'select':
						$type_id      = reset($this->Order->Menu->MenuType->getIdOfSlug('Select'));
						$type_ids     = $this->Order->Menu->MenuType->getIdOfSlug(array(
							'Appetizer',
							'Soup',
							'Sashimi',
							'Fish',
							'Meat',
							'Rice',
							'Dessert',
							'Append',
						));
						$body_id      = 'select';
						$wrap_class   = 'select';
						$render_view  = 'select';
						// Page.
						if (isset($this->request->query['p'])) {
							$this->set('p', $this->request->query['p']);
						}
						break;
				}
				// Get menu data.
				if (isset($type_ids)) {
					// $this->loadModel('Menu');
					$menu_type_names = $this->Order->Menu->MenuType->find('list');
					$menu_type_flags = $this->Order->Menu->MenuType->find('list', array('fields' => array('id', 'multiple')));
					if (array_search($this->request->pass[0], array('select')) !== false) {
						// Select menu combine.
						foreach ($type_ids as $id)
							$menus[$id] = $this->Order->Menu->getMenu($id);
					} else {
						$menus = $this->Order->Menu->getMenu($type_ids);
					}
					$this->set(compact('type_id', 'menu_type_names', 'menu_type_flags', 'menus', 'order_id', 'wrap_class'));
				}
				// Set body ID and wrap class.
				$this->set(compact('body_id', 'wrap_class'));
				// Render view.
				$this->render("{$render_view}");
			} else {
				// Set body ID.
				$this->set('body_id', 'top');
			}
		} elseif ($this->request->is('post') || $this->request->is('put')) {
			// Validation unbind.
			$this->Order->unbindValidation('keep', array());
			// Check order validation.
			if (isset($this->session['OrderMenu'])) {
				unset($this->session['Order']['menus']);
				unset($this->session['OrderMenu']);
				unset($this->session['MenuType']);
				unset($this->session['Menu']);
			}
			if ($this->_checkOrderValidation()) {
				// Success
				$this->redirect('/confirm');
			} else {
				// Failed.
				$this->redirect("/{$this->request->url}");
			}
		}
	}

	/**
	 * confirm method
	 * : 選択内容確認画面
	 *
	 * @return  void
	 * @url     /confirm
	 */
	public function confirm() {
		// Request type check.
		if ($this->request->is('get')) {
			// Order menus exist check .
			if (empty($this->session['OrderMenu']))
				$this->redirect('/');
			// Set body ID.
			$this->set('body_id', Inflector::variable($this->session['MenuType']['name_en']));
		} elseif ($this->request->is('post') || $this->request->is('put')) {
			// Registration
			$this->Order->OrderMenu->deleteAll(array('OrderMenu.order_id' => $this->session['Order']['id']));
			$save['Order']['id']           = $this->session['Order']['id'];
			$save['Order']['menu_type_id'] = $this->session['Order']['menu_type_id'];
			$save['Menu'] = array();
			foreach ($this->session['OrderMenu'] as $order_menu) {
				$save['Menu'][] = array(
					'OrderMenu' => array(
						'menu_id' => $order_menu['menu_id'],
					),
				);
			}
			// Validation unbind.
			$this->Order->unbindValidation('keep', array());
			// Save all.
			if ($this->Order->saveAll($save, array('deep' => true))) {
				// Success.
				$this->redirect('/complete');
			} else {
				// Failed.
				$this->Session->setFlash('登録に失敗しました。<br>初めからやり直してください。', 'alert', array('class' => 'error'));
				$this->redirect('/unauth');
			}
		}
	}

	/**
	 * complete method
	 * : 選択内容確定画面
	 *
	 * @return  void
	 * @url     /complete
	 */
	public function complete() {
		// Set body ID.
		$this->set('body_id', 'thanks');
	}

	/**
	 * printing method
	 * : 印刷画面
	 *
	 * @return  void
	 * @url     /printing
	 */
	public function printing() {
		// Check order ID.
		if (isset($this->request->query['id'])) {
			// Success.
			$order = $this->Order->find('first', array(
				'contain'    => $this->Order->contain,
				'conditions' => array(
					'Order.id' => $this->request->query['id'],
				),
			));
			$this->set(compact('order'));
			// Set body ID.
			$this->set('body_id', 'print');
			// Session delete.
			// $this->Session->delete('Session');
		} else {
			// Failed.
			$this->redirect('/unauth');
		}
	}

/**
 * Private methods.
 */
	/**
	 * _checkOrderInput method
	 * : 購入手続きの入力データの精査
	 *
	 * @return  boolean
	 */
	private function _checkOrderValidation() {
		// Set form values.
		$this->Order->set($this->request->data);
		if (!$this->Order->validates()) {
			// Validation error.
			$this->Session->setFlash('ご入力内容に不備がございます。', 'alert', array('class' => 'error'));
			return false;
		} else {
			// Quantity check.
			if (count($this->request->data['OrderMenu']) > 12) {
				$this->Session->setFlash('メニューの選択数が上限を超えています。', 'alert', array('class' => 'error'));
				return false;
			}
			// Merge order data.
			$this->session = Hash::merge($this->session, $this->request->data);
			// Get menu type data.
			$this->Order->Menu->MenuType->recursive = -1;
			$this->session['MenuType'] = reset($this->Order->Menu->MenuType->read(null, $this->session['Order']['menu_type_id']));
			// Get menu datas.
			foreach ($this->session['OrderMenu'] as $key => $value) {
				$this->session['Order']['menus'][] = $value['menu_id'];
				$menu = $this->Order->Menu->find('first', array(
					'contain'    => $this->Order->Menu->contain,
					'conditions' => array(
						'Menu.id' => $value['menu_id'],
					),
				));
				$this->session['Menu'][$key]                = reset($menu);
				$this->session['Menu'][$key]['MenuType']    = $menu['MenuType'];
				$this->session['Menu'][$key]['MenuProduct'] = $menu['MenuProduct'];
			}
			// Set order session.
			$this->Session->write('Session', $this->session);
			return true;
		}
	}

}
