<?php
/**
 * Control Panel controller.
 *
 * @copyright   Copyright nanasan Inc. (http://73web.net/)
 * @link        http://www.yamatoyahonten.com/ 大和屋本店
 * @package     app.Controller
 * @since       yamatoya.sys v 1.0
 */
App::uses('AppController', 'Controller');

/**
 * Control Panel controller
 */
class CpanelController extends AppController {

	/**
	 * Controller name
	 *
	 * @var     string
	 */
	public $name = 'Cpanel';

	/**
	 * Theme
	 *
	 * @var string
	 */
	public $theme = 'Cpanel';

	/**
	 * Layout
	 *
	 * @var string
	 */
	public $layout = 'default';

	/**
	 * Use model
	 *
	 * @var     mixed
	 */
	public $uses = array('Order');

	/**
	 * Components
	 *
	 * @var     array
	 */
	public $components = array(
		'Auth' => array(
			'authenticate'   => array(						// 認証方法設定
				'Form' => array(								// フォーム認証
					'userModel' => 'User',							// 認証用モデル
					'fields'    => array(							// 認証フィールドの変更
						'username'  => 'login_id',						// ログイン名
						'password'  => 'password',						// パスワード
					),
				),
			),
			'loginAction'    => '/admin/cpanel/login',		// ログイン画面の指定
			'loginRedirect'  => '/admin/cpanel/',			// ログイン後のリダイレクト先
			'logoutRedirect' => '/admin/cpanel/login',		// ログアウト後のリダイレクト先
		),
		'Search.Prg',
		// 'Security',
	);

	/**
	 * Helpers
	 *
	 * @var     array
	 */
	public $helpers = array(
		'Form'      => array('className' => 'BoostCake.BoostCakeForm'),
		'Html'      => array('className' => 'BoostCake.BoostCakeHtml'),
		'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
	);

	/**
	 * Paginate setting
	 *
	 * @var     mixed
	 */
	public $paginate = array(
		'Order' => array(
			'contain' => array(
				'Menu' => array(
					'MenuProduct' => array(
						'Product' => array(
							'ProductType.name',
						),
					),
				),
				'MenuType.name',
			),
			'order' => array('Order.created' => 'DESC'),
			'limit' => 10,
		),
	);

	/**
	 * Preset variables
	 *
	 * @var array
	 */
	public $presetVars = array(
		array(
			'field' => 'order_name',
			'type'  => 'value',
//			'empty' => true,
		),
		array(
			'field' => 'keyword',
			'type'  => 'value',
//			'empty' => true,
		),
		array(
			'field' => 'menu_type_id',
			'type'  => 'value',
//			'empty' => true,
		),
		array(
			'field' => 'date_from',
			'type'  => 'value',
//			'empty' => true,
		),
		array(
			'field' => 'date_to',
			'type'  => 'value',
//			'empty' => true,
		),
	);

/**
 * Controller hook methods.
 */
	/**
	 * beforeFilter method
	 *
	 * @return  void
	 */
	public function beforeFilter() {
		// Auth Component settings.
		AuthComponent::$sessionKey = "Auth.Administrator";					// ログインセッションの保存キー
		$this->Auth->loginError    = 'IDまたはパスワードが違います。';		// ログインエラー時
		$this->Auth->authError     = 'ログインが必要です。';				// 認証が必要なページのアクセス時
		$this->Auth->deny();												// 全アクションの認証必須指定(deny all)
		// Security Component settings.
		// $this->Security->unlockedActions   = array('index');
		// $this->Security->validatePost      = false;
		// $this->Security->blackHoleCallback = '_blackHole';
		// Set logged user data.
		if ($this->action != 'login') {
			// Get logged user data.
			$this->administrator = $this->Auth->user();
			$this->set('administrator', $this->administrator);
		}
	}

/**
 * Security Component methods.
 */
	/**
	 * _blackHole method
	 *
	 * @return  void
	 */
	public function _blackHole() {
		// Redirect.
		$this->redirect('/');
	}

/**
 * Action methods.
 */
	/**
	 * admin_login method
	 * : ログイン画面
	 *
	 * @return  void
	 * @url     /admin/cpanel/login
	 */
	public function admin_login() {
		// Check the authenticated.
		if (!$this->Auth->loggedIn()) {
			// Check the login request.
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->Auth->login()) {
					// Success.
					$this->redirect($this->Auth->redirectUrl());
				} else {
					// Failed.
					$this->Session->setFlash($this->Auth->loginError, 'alert', array('class' => 'danger'));
				}
			}
		} else {
			// Logged in.
			$this->redirect($this->Auth->loginRedirect);
		}
		// Sign-in layout.
		$this->layout = 'signin';
		// Render view.
		$this->render('login/index');
	}

	/**
	 * admin_logout method
	 * : ログアウト処理
	 *
	 * @return  void
	 * @url     /admin/cpanel/logout
	 */
	public function admin_logout() {
		// Logout process.
		$this->redirect($this->Auth->logout());
	}

	/**
	 * admin_index method
	 * : トップ(予約一覧へリダイレクト)
	 *
	 * @return  void
	 * @url     /admin/cpanel
	 */
	public function admin_index() {
		// Redirect list.
		$this->redirect('/admin/cpanel/order_list');
	}

	/**
	 * admin_order_list method
	 * : 予約一覧
	 *
	 * @return  void
	 * @url     /admin/cpanel/order_list
	 */
	public function admin_order_list() {
		// Get order datas.
		$this->Prg->commonProcess();
		$search = $this->passedArgs;
		if (!empty($search['date_from']))
			$search['date_from'] = "{$search['date_from']} 00:00:00";
		if (!empty($search['date_to']))
			$search['date_to']   = "{$search['date_to']} 23:59:59";
		$orders = $this->paginate('Order', $this->Order->parseCriteria($search));
		// Get pulldown datas.
		$pulldown = array(
			'MenuType' => $this->Order->MenuType->getSelect(),
		);
		// Set datas.
		$this->set(compact('orders', 'pulldown'));
		// Render view.
		$this->render("order/list");
	}

	/**
	 * admin_order_entry method
	 * : 新規予約作成
	 *
	 * @return  void
	 * @url     /admin/cpanel/order_entry
	 */
	public function admin_order_entry() {
		// Registration.
		$this->_regist_order();
		// Render view.
		$this->render("order/entry");
	}

	/**
	 * admin_order_detail method
	 * : 予約情報詳細
	 *
	 * @param   integer $id
	 * @return  void
	 * @url     /admin/cpanel/order_detail
	 */
	public function admin_order_detail($id = null) {
		// Get order data.
		$this->_read_order($id);
		// Render view.
		$this->render("order/detail");
	}

	/**
	 * admin_order_edit method
	 * : 予約情報編集
	 *
	 * @param   integer $id
	 * @return  void
	 * @url     /admin/cpanel/order_edit
	 */
	public function admin_order_edit($id = null) {
		// Registration.
		$this->_regist_order();
		// Get order data.
		if ($this->request->is('get')) {
			$this->request->data = $this->_read_order($id);
		} elseif ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Order']['id'] = $id;
			$this->request->data['Menu'] = array();
		}
		// Get pulldown datas.
		$pulldown = array(
			'MenuType' => $this->Order->MenuType->getSelect(),
			'Menu'     => $this->Order->Menu->getMenuListByType(),
			'Course'   => $this->Order->Menu->getMenu($this->Order->MenuType->getChildIdOfSlug('Course')),
			'Select'   => $this->Order->Menu->getMenuIndexId($this->Order->MenuType->getChildIdOfSlug('Select')),
			'SelectN'  => $this->Order->MenuType->getChildNameListOfSlug('Select'),
			'SelectM'  => $this->Order->MenuType->getChildMultipleListOfSlug('Select'),
			'SelectR'  => $this->Order->Menu->getRequiredList($this->Order->MenuType->getChildIdOfSlug('Select')),
		);
		// Set datas.
		$this->set(compact('pulldown'));
		// Render view.
		$this->render("order/edit");
	}

/**
 * Private methods.
 */
	/**
	 * _read_order method
	 * : 予約情報単体取得処理
	 *
	 * @param   integer $id
	 * @return  array
	 */
	private function _read_order($id = null) {
		// Get order data.
		if ($this->Order->exists($id)) {
			// Set order data.
			$this->Order->id = $id;
			$this->Order->contain($this->Order->contain);
			$order = $this->Order->read();
			// Set history order data.
			$order = unserialize($order['Order']['history']);
			$this->set(compact('order'));
		} else {
			// Not found.
			$this->Session->setFlash('指定されたデータありません。', 'alert', array('class' => 'danger'));
			$this->redirect('/admin/cpanel/order_list');
		}
		return $order;
	}

	/**
	 * _regist_order method
	 * : 予約情報保存処理
	 *
	 * @return  void
	 */
	private function _regist_order() {
		// Registration.
		if ($this->request->is('post') || $this->request->is('put')) {
			// Validation
			$this->Order->set($this->request->data);
			if (!$this->Order->validates()) {
				// Validation error.
				$this->Session->setFlash('登録に失敗しました。', 'alert', array('class' => 'danger'));
				return false;
			}
			// Delete order menus.
			if ($this->request->is('put')) {
				if (empty($this->request->data['Order']['menu_type_id']))
					$this->Order->OrderMenu->deleteAll(array('OrderMenu.order_id' => $this->request->data['Order']['id']));
			}
			// Save all.
			$this->Order->modeAdmin(true);
			if ($this->Order->saveAll($this->request->data, array('deep' => true))) {
				// Success.
				if ($this->request->is('post')) {
					$this->Session->setFlash('新しい予約を追加しました。', 'alert', array('class' => 'success'));
					$this->redirect("/admin/cpanel/order_list");
				} elseif ($this->request->is('put')) {
					$this->Session->setFlash('予約情報を更新しました。', 'alert', array('class' => 'success'));
					$this->redirect("/admin/cpanel/order_detail/{$this->request->data['Order']['id']}");
				}
			} else {
				// Failed.
				$this->Session->setFlash('登録に失敗しました。', 'alert', array('class' => 'danger'));
			}
		}
	}

}
