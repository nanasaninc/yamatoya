/*
 Navicat Premium Data Transfer

 Source Server         : 大和屋本店 (デモ)
 Source Server Type    : MySQL
 Source Server Version : 50613
 Source Host           : mysql131.heteml.jp
 Source Database       : _yamatoya_test

 Target Server Type    : MySQL
 Target Server Version : 50613
 File Encoding         : utf-8

 Date: 02/05/2014 16:22:03 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `menu_products`
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_id` int(11) unsigned NOT NULL COMMENT 'メニューID',
  `product_id` int(11) unsigned NOT NULL COMMENT '商品(料理/サービス)ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='メニュー商品関連テーブル';

-- ----------------------------
--  Table structure for `menu_types`
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `name_en` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称(English)',
  `multiple` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '複数選択フラグ',
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT '親ID',
  `lft` int(11) unsigned DEFAULT NULL COMMENT '左座標',
  `rght` int(11) unsigned DEFAULT NULL COMMENT '右座標',
  `created` datetime DEFAULT NULL COMMENT '登録日時',
  `modified` datetime DEFAULT NULL COMMENT '更新日時',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='メニュー種別テーブル';

-- ----------------------------
--  Records of `menu_types`
-- ----------------------------
BEGIN;
INSERT INTO `menu_types` VALUES ('1', 'コースメニュー', 'Course', '0', null, '1', '8', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('2', '日本料理', 'Japanese', '0', '1', '2', '3', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('3', '西洋料理', 'French', '0', '1', '4', '5', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('4', '中国料理', 'Chinese', '0', '1', '6', '7', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('5', 'セレクトメニュー', 'Select', '0', null, '9', '26', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('6', '前菜', 'Appetizer', '0', '5', '10', '11', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('7', '吸い物', 'Soup', '0', '5', '12', '13', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('8', 'お造り', 'Sashimi', '0', '5', '14', '15', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('9', '魚料理', 'Fish', '0', '5', '16', '17', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('10', '肉料理', 'Meat', '0', '5', '18', '19', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('11', '御飯物', 'Rice', '0', '5', '20', '21', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('12', 'デザート', 'Dessert', '0', '5', '22', '23', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('13', '追加料理', 'Append', '1', '5', '24', '25', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null);
COMMIT;

-- ----------------------------
--  Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_type_id` int(11) unsigned NOT NULL COMMENT 'メニュー種別ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `kana` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称(かな)',
  `price` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '税抜き価格',
  `standard_price` int(11) unsigned DEFAULT NULL COMMENT '基準価格',
  `description` text COMMENT '説明',
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '必須フラグ',
  `image` varchar(255) DEFAULT NULL COMMENT '画像名',
  `created` datetime DEFAULT NULL COMMENT '登録日時',
  `modified` datetime DEFAULT NULL COMMENT '更新日時',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='メニューテーブル';

-- ----------------------------
--  Table structure for `order_menus`
-- ----------------------------
DROP TABLE IF EXISTS `order_menus`;
CREATE TABLE `order_menus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_id` int(11) unsigned NOT NULL COMMENT '予約ID',
  `menu_id` int(11) unsigned NOT NULL COMMENT 'メニューID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='予約情報メニュー関連テーブル';

-- ----------------------------
--  Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `order_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'ご予約ID',
  `datetime` datetime NOT NULL COMMENT 'ご予約日時',
  `groom_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '新郎氏名(姓)',
  `bride_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '新婦氏名(旧姓)',
  `menu_type_id` int(11) unsigned DEFAULT NULL COMMENT 'メニュー種別ID',
  `comment1` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '備考1',
  `comment2` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT '備考2',
  `created` datetime DEFAULT NULL COMMENT '登録日時',
  `modified` datetime DEFAULT NULL COMMENT '更新日時',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='予約情報テーブル';

-- ----------------------------
--  Table structure for `prefectures`
-- ----------------------------
DROP TABLE IF EXISTS `prefectures`;
CREATE TABLE `prefectures` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='都道府県テーブル';

-- ----------------------------
--  Records of `prefectures`
-- ----------------------------
BEGIN;
INSERT INTO `prefectures` VALUES ('1', '北海道'), ('2', '青森県'), ('3', '岩手県'), ('4', '宮城県'), ('5', '秋田県'), ('6', '山形県'), ('7', '福島県'), ('8', '茨城県'), ('9', '栃木県'), ('10', '群馬県'), ('11', '埼玉県'), ('12', '千葉県'), ('13', '東京都'), ('14', '神奈川県'), ('15', '新潟県'), ('16', '富山県'), ('17', '石川県'), ('18', '福井県'), ('19', '山梨県'), ('20', '長野県'), ('21', '岐阜県'), ('22', '静岡県'), ('23', '愛知県'), ('24', '三重県'), ('25', '滋賀県'), ('26', '京都府'), ('27', '大阪府'), ('28', '兵庫県'), ('29', '奈良県'), ('30', '和歌山県'), ('31', '鳥取県'), ('32', '島根県'), ('33', '岡山県'), ('34', '広島県'), ('35', '山口県'), ('36', '徳島県'), ('37', '香川県'), ('38', '愛媛県'), ('39', '高知県'), ('40', '福岡県'), ('41', '佐賀県'), ('42', '長崎県'), ('43', '熊本県'), ('44', '大分県'), ('45', '宮崎県'), ('46', '鹿児島県'), ('47', '沖縄県');
COMMIT;

-- ----------------------------
--  Table structure for `product_types`
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `menu_type_id` int(11) unsigned NOT NULL COMMENT 'メニュー種別ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `slug` varchar(255) DEFAULT NULL,
  `sort` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '順序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品(料理/サービス)種別テーブル';

-- ----------------------------
--  Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `product_type_id` int(11) unsigned DEFAULT NULL COMMENT '商品(料理/サービス)種別ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `name_ruby` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称(ルビ出力)',
  `description` varchar(255) DEFAULT NULL COMMENT '説明',
  `image` varchar(255) DEFAULT NULL COMMENT '画像名',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品(料理/サービス)テーブル';

-- ----------------------------
--  Table structure for `states`
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='予約状況テーブル';

-- ----------------------------
--  Records of `states`
-- ----------------------------
BEGIN;
INSERT INTO `states` VALUES ('1', '仮予約'), ('2', '予約確定'), ('3', '終了'), ('4', '取消し');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `login_id` varchar(255) NOT NULL DEFAULT '' COMMENT 'ログインID',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT 'パスワード',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '名前',
  `created` datetime DEFAULT NULL COMMENT '登録日時',
  `modified` datetime DEFAULT NULL COMMENT '更新日時',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `deleted_date` datetime DEFAULT NULL COMMENT '削除日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='ログインユーザーテーブル';

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', '73staff', '00e8bc06509498ff36a427f67681990ea99c36b4', '73#スタッフ', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null), ('2', 'yamatoya', '5bfb3ed3f8f3e69d79ac2389721e1dbff349b9fb', '大和屋スタッフ', '2013-11-01 09:00:00', '2013-11-01 09:00:00', '0', null);
COMMIT;

-- ----------------------------
--  View structure for `view_menus`
-- ----------------------------
DROP VIEW IF EXISTS `view_menus`;
CREATE VIEW `view_menus` AS select `menu_types_c`.`name` AS `menu_type`,`menus`.`name` AS `menu_name`,`product_types`.`name` AS `product_type`,`products`.`name` AS `name`,`products`.`name_ruby` AS `name_ruby`,`menus`.`price` AS `price`,(`menus`.`price` - `menus`.`standard_price`) AS `difference_price` from (((((`menu_types` `menu_types_p` left join `menu_types` `menu_types_c` on((`menu_types_p`.`id` = `menu_types_c`.`parent_id`))) left join `menus` on((`menu_types_c`.`id` = `menus`.`menu_type_id`))) left join `menu_products` on((`menus`.`id` = `menu_products`.`menu_id`))) left join `products` on((`menu_products`.`product_id` = `products`.`id`))) left join `product_types` on((`products`.`product_type_id` = `product_types`.`id`))) where (`menu_types_c`.`parent_id` is not null);

-- ----------------------------
--  View structure for `view_types`
-- ----------------------------
DROP VIEW IF EXISTS `view_types`;
CREATE VIEW `view_types` AS select `c`.`id` AS `id`,`p`.`name` AS `type`,`p`.`name_en` AS `type_en`,`c`.`name` AS `name`,`c`.`name_en` AS `name_en` from (`menu_types` `p` join `menu_types` `c` on((`p`.`id` = `c`.`parent_id`)));

SET FOREIGN_KEY_CHECKS = 1;
